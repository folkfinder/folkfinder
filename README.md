# FolkFinder

Application to find your friends with face reckognition.

![FolkFinder](https://www.dropbox.com/s/9rarbq4jsc2abl5/FolkFinder.jpg?raw=1)  

Install a [release](https://gitlab.com/magnusmj/folkfinder/-/releases)  
or  
## To install Dependencies needed:

### Debian/Ubuntu/derivatives:
```bash
sudo apt install python3-pip git python3-setuptools
sudo pip3 install pyqt5 opencv-python numpy imutils dateutils imagesize appdirs
```

### Arch:
```bash
sudo pacman -S python-pip git python-setuptools
sudo pip install pyqt5 opencv-python numpy imutils dateutils imagesize appdirs
```

### Windows:  

Install [git](https://git-scm.com/download/win)  
Install [python3](https://www.python.org/ftp/python/3.7.4/Python-3.7.4.tar.xz)  

open command prompt and type:
```cmd
pip install pyqt5 opencv-python numpy imutils dateutils imagesize
```

## to get it:

```bash
git clone https://gitlab.com/magnusmj/folkfinder.git
```

## to run it:

```bash
cd folkfinder
python3 main.py
```

## to install it (Linux):

```bash
sudo make install
```

## to uninstall it (Linux):

```bash
sudo make clean
```

