#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='folkfinder_pkg',
	version='0.1.2',
	license="MIT",
	description='Application to find your friends with face reckognition',
	author='Magnus Jørgensen',
	author_email='magnusmj@gmail.com',
	url='https://gitlab.com/folkfinder/folkfinder',
	entry_points={
		'console_scripts': [
			'folkfinder = folkfinder_pkg.__main__:main',
		],
		'gui_scripts': [
			'folkfinder = folkfinder_pkg.__main__:main',
		]
	},
	packages=find_packages(),
	package_data={
		'': ['*.png', '*.css', '*.prototxt', '*.caffemodel'],
	},
	install_requires=[
		'pyqt5', 'opencv-python', 'numpy', 'imutils', 'dateutils', 'imagesize', 'appdirs'
	]
)

