# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from folkfinder_pkg.DATA.Category import Category
from folkfinder_pkg.DATA.ChangeObject import ChangeObject
from folkfinder_pkg.LOGIC.Events import ChangeEvent


class Categories(ChangeObject):
	def __init__(self, data_store):
		ChangeObject.__init__(self)
		self._data_store = data_store
		self._items = {}

	def add_category(self, category_name, flags):
		category = Category(self._data_store, category_name, flags)
		self.changed(ChangeEvent(self, ChangeEvent.BeforeObjectAdded, category))
		self._add_item(category)
		self.changed(ChangeEvent(self, ChangeEvent.ObjectAdded, category))

	def remove_category(self, category):
		if category.uid in self._items:
			self.changed(ChangeEvent(self, ChangeEvent.BeforeObjectRemoved, category))
			self._remove_item(category)
			self.changed(ChangeEvent(self, ChangeEvent.ObjectRemoved, category))

	def _add_item(self, item):
		self._items[item.uid] = item

	def _remove_item(self, item):
		self._items.pop(item.uid)

	@property
	def items(self):
		return list(self._items.values())

	def get_by_id(self, id):
		if id in self._items:
			return self._items[id]
		return None

	@property
	def image_categories(self):
		categories = []
		for category in self.items:
			if category.is_image_category:
				categories.append(category)
		return categories

	@property
	def identity_categories(self):
		categories = []
		for category in self.items:
			if category.is_identity_category:
				categories.append(category)
		return categories

	@property
	def location_categories(self):
		categories = []
		for category in self.items:
			if category.is_location_category:
				categories.append(category)
		return categories

	@property
	def chronology_categories(self):
		categories = []
		for category in self.items:
			if category.is_chronology_category:
				categories.append(category)
		return categories

	@staticmethod
	def default(data_store):
		return Categories(data_store)

	def serialize_json(self):
		return \
			{
				'items': self.items
			}

	@staticmethod
	def deserialize(data, data_store):
		if data is None:
			return Categories.default(data_store)
		item = Categories(data_store)
		item.deserialize_data(data)
		return item

	def deserialize_data(self, data):
		for item in data['items']:
			category_item = Category.deserialize(item, self._data_store)
			self._add_item(category_item)