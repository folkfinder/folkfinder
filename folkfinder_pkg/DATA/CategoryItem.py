# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from folkfinder_pkg.DATA.ChangeObject import IdObject
from folkfinder_pkg.DATA.ItemReferencer import ItemReferencer
from folkfinder_pkg.LOGIC.Events import ValueChangeEvent


class CategoryItem(ItemReferencer, IdObject):
	def __init__(self, data_store, category, category_item_name="new item"):
		ItemReferencer.__init__(self, data_store)
		IdObject.__init__(self)
		self._category = category
		self._name = category_item_name

	@property
	def name(self):
		return self._name

	@name.setter
	def name(self, value):
		old_val = self._name
		self._name = str(value)
		self.changed(ValueChangeEvent(self, 'name', old_val, self._name))

	@property
	def category(self):
		return self._category

	@property
	def owner(self):
		return self._category

	@property
	def category_name(self):
		return self._category.name

	def serialize_json(self):
		return \
			{
				'uid': IdObject.serialize_json(self),
				'ir': ItemReferencer.serialize_json(self),
				'name': self._name,
			}

	@staticmethod
	def deserialize(category, data, data_store):
		item = CategoryItem(data_store, category)
		item.deserialize_data(data)
		return item

	def deserialize_data(self, data):
		IdObject.deserialize_data(self, data['uid'])
		ItemReferencer.deserialize_data(self, data['ir'])
		self._name = data.get('name', "")