# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from folkfinder_pkg.DATA.ChangeObject import ChangeObject
from folkfinder_pkg.DATA.Face import Face
from folkfinder_pkg.LOGIC.Events import ChangeEvent


class Faces(ChangeObject):
	def __init__(self, data_store):
		ChangeObject.__init__(self)
		self._data_store = data_store
		self._items = {}

	def add_face(self, image, pos, size):
		face = Face(self._data_store, image, pos, size)
		self.changed(ChangeEvent(self, ChangeEvent.BeforeObjectAdded, face))
		self._items[face.uid] = face
		self.changed(ChangeEvent(self, ChangeEvent.ObjectAdded, face))
		return face

	def add_face_object(self, face):
		self.changed(ChangeEvent(self, ChangeEvent.BeforeObjectAdded, face))
		self._items[face.uid] = face
		self.changed(ChangeEvent(self, ChangeEvent.ObjectAdded, face))

	def remove_face(self, face):
		if face.uid in self._items:
			self.changed(ChangeEvent(self, ChangeEvent.BeforeObjectRemoved, face))
			self._items.pop(face.uid)
			self.changed(ChangeEvent(self, ChangeEvent.ObjectRemoved, face))

	def get_by_id(self, id):
		if id in self._items:
			return self._items[id]
		return None

	@staticmethod
	def default(data_store):
		return Faces(data_store)

	def serialize_json(self):
		return \
			{
				'items': list(self._items.values())
			}

	@staticmethod
	def deserialize(data, data_store):
		item = Faces(data_store)
		if not data is None:
			item.deserialize_data(data)
		return item

	def deserialize_data(self, data):
		for item_data in data['items']:
			face_item = Face.deserialize(item_data, self._data_store)
			self._items[face_item.uid] = face_item