# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from folkfinder_pkg.DATA.CategoryItem import CategoryItem
from folkfinder_pkg.DATA.ChangeObject import ChangeObject, IdObject
from folkfinder_pkg.LOGIC.Events import ChangeEvent, ValueChangeEvent


class Category(ChangeObject, IdObject):
	Image = 1
	Identity = 2
	Location = 4
	Chronology = 8
	Nested = 16

	def __init__(self, data_store, category_name="new_category", flags=0):
		ChangeObject.__init__(self)
		IdObject.__init__(self)
		self._name = category_name
		self._items = {}
		self._flags = flags
		self._data_store = data_store

	@property
	def name(self):
		return self._name

	@name.setter
	def name(self, value):
		old_val = self._name
		self._name = value
		self.changed(ValueChangeEvent(self, 'name', old_val, self._name))

	@property
	def is_image_category(self):
		return  bool(Category.Image & self._flags)

	@is_image_category.setter
	def is_image_category(self, value):
		if self.is_image_category != bool(value):
			old_val = self._flags
			if self.is_image_category:
				self._flags -= Category.Image
			else:
				self._flags += Category.Image
			self.changed(ValueChangeEvent(self, 'flags', old_val, self._flags))

	@property
	def is_identity_category(self):
		return bool(Category.Identity & self._flags)

	@is_identity_category.setter
	def is_identity_category(self, value):
		if self.is_identity_category != bool(value):
			old_val = self._flags
			if self.is_identity_category:
				self._flags -= Category.Identity
			else:
				self._flags += Category.Identity
			self.changed(ValueChangeEvent(self, 'flags', old_val, self._flags))

	@property
	def is_location_category(self):
		return bool(Category.Location & self._flags)

	@is_location_category.setter
	def is_location_category(self, value):
		if self.is_location_category != bool(value):
			old_val = self._flags
			if self.is_location_category:
				self._flags -= Category.Location
			else:
				self._flags += Category.Location
			self.changed(ValueChangeEvent(self, 'flags', old_val, self._flags))

	@property
	def is_chronology_category(self):
		return bool(Category.Chronology & self._flags)

	@is_chronology_category.setter
	def is_chronology_category(self, value):
		if self.is_chronology_category != bool(value):
			old_val = self._flags
			if self.is_chronology_category:
				self._flags -= Category.Chronology
			else:
				self._flags += Category.Chronology
			self.changed(ValueChangeEvent(self, 'flags', old_val, self._flags))

	@property
	def is_nested_category(self):
		return bool(Category.Nested & self._flags)

	@property
	def items(self):
		return list(self._items.values())

	def add_category_item(self, name):
		category_item = CategoryItem(self._data_store, self, name)
		self.changed(ChangeEvent(self, ChangeEvent.BeforeObjectAdded, category_item))
		self._items[category_item.uid] = category_item
		self.changed(ChangeEvent(self, ChangeEvent.ObjectAdded, category_item))
		return category_item

	def get_by_id(self, id):
		if id in self._items:
			return self._items[id]
		return None

	def serialize_json(self):
		return \
			{
				'uid': IdObject.serialize_json(self),
				'name': self._name,
				'items': list(self._items.values()),
				'flags': self._flags
			}

	@staticmethod
	def deserialize(data, data_store):
		item = Category(data_store)
		item.deserialize_data(data)
		return item

	def deserialize_data(self, data):
		IdObject.deserialize_data(self, data['uid'])
		self._name = data.get('name', "")
		self._flags = data.get('flags', 0)
		for item in data.get('items', []):
			category_item = CategoryItem.deserialize(self, item, self._data_store)
			self._items[category_item.uid] = category_item

