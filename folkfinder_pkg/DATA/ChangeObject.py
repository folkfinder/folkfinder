# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import uuid


class IdObject(object):
	def __init__(self):
		self._uid = uuid.uuid1().urn[9:]

	@property
	def uid(self):
		return self._uid

	def serialize_json(self):
		return {'uid': self._uid}

	def deserialize_data(self, data):
		self._uid = data['uid']


class ChangeObject(object):
	def __init__(self):
		self._change_handlers = []

	def add_change_handler(self, handler):
		self._change_handlers.append(handler)

	def remove_change_handler(self, change_handler):
		try:
			self._change_handlers.remove(change_handler)
		except (KeyError, ValueError):
			pass

	def changed(self, event):
		for handler in list(self._change_handlers):
			handler(event)
