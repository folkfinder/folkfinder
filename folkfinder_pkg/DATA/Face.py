# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from folkfinder_pkg.DATA.ChangeObject import IdObject, ChangeObject
from folkfinder_pkg.DATA.ItemReference import ItemReference
from folkfinder_pkg.LOGIC.Events import ValueChangeEvent


class Face(ChangeObject, IdObject):
	def __init__(self, data_store, image=None, pos=None, size=None, confidence=100):
		IdObject.__init__(self)
		ChangeObject.__init__(self)
		self._data_store = data_store
		if not image is None:
			self._image_reference = ItemReference(data_store, ItemReference.ImageReference, [image.uid, image.uid])
		self._pos = pos
		self._size = size
		self._identity_reference = None
		self._confidence = confidence

	@property
	def image(self):
		return self._image_reference.get_item()

	@property
	def identity(self):
		if self._identity_reference is None:
			return None
		return self._identity_reference.get_item()

	@property
	def owner(self):
		return self

	@property
	def name(self):
		image = self.image
		image_name = "None"
		if not image is None:
			image_name = image.name
		identity = self.identity
		if identity is None:
			return "Unknown on " + image_name
		return identity.name + " on " + image_name

	@property
	def size(self):
		return self._size

	@property
	def pos(self):
		return self._pos

	def inside(self, pos):
		dist = max(abs(self._pos[0] - pos[0]), abs(self._pos[1] - pos[1]))
		if dist < min(self._size[0], self._size[1])/2:
			return True
		return False

	def set_identity(self, identity):
		if self._identity_reference is None:
			old_val = self._identity_reference
		else:
			old_val = self._identity_reference.get_item()
		self._identity_reference = ItemReference(self._data_store, ItemReference.IdentityReference, [identity.uid, identity.uid])
		self.changed(ValueChangeEvent(self, 'identity', old_val, identity))

	def serialize_json(self):
		return \
			{
				'uid': IdObject.serialize_json(self),
				'img_ref': self._image_reference,
				'id_ref': self._identity_reference,
				'pos': self._pos,
				'size': self._size
			}

	@staticmethod
	def deserialize(data, data_store):
		item = Face(data_store)
		item.deserialize_data(data)
		return item

	def deserialize_data(self, data):
		IdObject.deserialize_data(self, data['uid'])
		self._image_reference = ItemReference.deserialize(data.get('img_ref', None), self._data_store)
		self._identity_reference = ItemReference.deserialize(data.get('id_ref', None), self._data_store)
		self._pos = data.get('pos', [0.0, 0.0])
		self._size = data.get('size', [0.0, 0.0])


