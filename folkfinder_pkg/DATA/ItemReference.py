# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from folkfinder_pkg.DATA.ChangeObject import ChangeObject, IdObject


class ItemReference(ChangeObject, IdObject):
	ImageReference = 0
	PersonReference = 1
	FaceReference = 2
	CategoryReference = 3
	IdentityReference = 4

	def __init__(self, data_store, type=0, data=None):
		ChangeObject.__init__(self)
		IdObject.__init__(self)
		self._data_store = data_store
		self._data = data
		self._type = type

	@property
	def data(self):
		return self._data

	@property
	def name(self):
		return self.get_item().name

	@name.setter
	def name(self, value):
		attr = getattr(type(self.get_item()), 'name', None)
		if attr.fset is not None:
			self.get_item().name = value

	def get_item(self):
		if self._type == ItemReference.ImageReference:
			return self._data_store.images.get_image_by_uid(self._data[0])
		elif self._type == ItemReference.CategoryReference:
			category = self._data_store.categories.get_by_id(self._data[1])
			return category.get_by_id(self._data[0])
		elif self._type == ItemReference.FaceReference:
			return self._data_store.faces.get_by_id(self._data[0])
		elif self._type == ItemReference.IdentityReference:
			return self._data_store.identities.get_by_id(self._data[0])

	def serialize_json(self):
		return \
			{
				'uid': IdObject.serialize_json(self),
				'type': self._type,
				'data': self._data
			}

	@staticmethod
	def deserialize(data, data_store):
		if data is None:
			return None
		item = ItemReference(data_store)
		item.deserialize_data(data)
		return item

	def deserialize_data(self, data):
		IdObject.deserialize_data(self, data['uid'])
		self._type = data.get('type', ItemReference.ImageReference)
		self._data = data.get('data', None)