# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from folkfinder_pkg import __version__
from folkfinder_pkg.DATA.ChangeObject import ChangeObject


class Settings(ChangeObject):
	def __init__(self):
		ChangeObject.__init__(self)
		self._version = __version__
		self._image_library_path = ""

	@staticmethod
	def default():
		settings = Settings()
		settings._version = __version__
		settings._image_library_path = ""
		return settings

	@property
	def version(self):
		return str(self._version)

	@property
	def images_path(self):
		return self._image_library_path

	@property
	def image_library_path(self):
		return self._image_library_path

	@image_library_path.setter
	def image_library_path(self, value):
		self._image_library_path = value

	def serialize_json(self):
		return \
			{
				'version': self._version,
				'img_lib_path': self._image_library_path
			}

	@staticmethod
	def deserialize(data):
		item = Settings()
		item.deserialize_data(data)
		return item

	def deserialize_data(self, data):
		self._version = data.get('version', __version__)
		self._image_library_path = data.get('img_lib_path', "")