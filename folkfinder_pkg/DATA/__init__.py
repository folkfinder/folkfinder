# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import json
import zlib

text_formats_endings = ['.json']


def complex_handler(Obj):
	if hasattr(Obj, 'serialize_json'):
		return Obj.serialize_json()
	else:
		raise Exception('Object of type %s with value of %s is not JSON serializable' % (type(Obj), repr(Obj)))


def read_text_from_disk(file_path):
	with open(file_path) as data_file:
		data = data_file.read()
	return data


def read_lines_from_disk(file_path):
	with open(file_path) as data_file:
		data = data_file.readlines()
	return data


def read_json_data_from_disk(file_path):
	is_binary = True
	for ending in text_formats_endings:
		if ending in file_path.lower():
			is_binary = False
	if not is_binary:
		with open(file_path) as data_file:
			data = json.load(data_file)
	else:
		with open(file_path, "rb") as data_file:
			S = data_file.read()
			data_string = zlib.decompress(S)
			data = json.loads(data_string.decode("UTF-8"))
	return data


def write_data_to_disk(file_path, data):
	is_binary = True
	for ending in text_formats_endings:
		if ending in file_path.lower():
			is_binary = False
	if not is_binary:
		text_file = open(file_path, "w")
		text_file.write(data)
		text_file.close()
	else:
		code = zlib.compress(bytes(data, 'UTF-8'))
		text_file = open(file_path, "wb")
		text_file.write(code)
		text_file.close()


def get_uids(idobject_list):
	uids = []
	for idobject in idobject_list:
		uids.append(idobject.uid)
	return uids
