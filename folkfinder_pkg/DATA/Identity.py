# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from datetime import datetime

from dateutil.relativedelta import relativedelta

from folkfinder_pkg.DATA.CategoryItem import CategoryItem
from folkfinder_pkg.DATA.ChangeObject import  IdObject
from folkfinder_pkg.DATA.Face import Face
from folkfinder_pkg.DATA.ItemReference import ItemReference
from folkfinder_pkg.DATA.ItemReferencer import ItemReferencer


class Identity(ItemReferencer, IdObject):
	_normalized_faces = []

	def __init__(self, data_store, full_name="new_id", age=0):
		ItemReferencer.__init__(self, data_store)
		IdObject.__init__(self)
		self._full_name = full_name
		self._birth_timestamp = datetime.now() - relativedelta(years=age)


	@property
	def owner(self):
		return self

	@property
	def name(self):
		return self._full_name

	@name.setter
	def name(self, value):
		if self._full_name != "Unknown":
			self._full_name = value

	@property
	def age(self):
		return int((datetime.now() - self._birth_timestamp).days / 365.25)

	@age.setter
	def age(self, value):
		self._birth_timestamp = datetime.now() - relativedelta(years=int(value))

	@property
	def date_of_birth(self):
		return str(self._birth_timestamp)

	def add_face(self, face):
		self.add_item_reference(face, ItemReference.FaceReference)

	def get_faces(self):
		faces = []
		for item in self.get_referenced_items():
			if type(item) is Face:
				faces.append(item)
		return faces

	def get_category_items(self):
		items = []
		for item in self.get_referenced_items():
			if type(item) is CategoryItem:
				items.append(item)
		return items

	def serialize_json(self):
		return \
			{
				'uid': IdObject.serialize_json(self),
				'refs': ItemReferencer.serialize_json(self),
				'full_name': self._full_name,
				'dob': self._birth_timestamp.timestamp(),
				'nfaces': self._normalized_faces,

			}

	@staticmethod
	def deserialize(data, data_store):
		item = Identity(data_store)
		item.deserialize_data(data)
		return item

	def deserialize_data(self, data):
		IdObject.deserialize_data(self, data['uid'])
		ItemReferencer.deserialize_data(self, data.get('refs', None))
		self._full_name = data.get('full_name', "")
		self._birth_timestamp = datetime.fromtimestamp(data.get('dob', datetime.now().timestamp()))
		self._normalized_faces = data.get('nfaces', [])
