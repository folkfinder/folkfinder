# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from folkfinder_pkg.DATA.Faces import Faces
from folkfinder_pkg.DATA.Categories import Categories
from folkfinder_pkg.DATA.ChangeObject import ChangeObject
from folkfinder_pkg.DATA.Identities import Identities
from folkfinder_pkg.DATA.Images import Images
from folkfinder_pkg.DATA.Settings import Settings


class DataStore(ChangeObject):
	_identities: Identities
	_settings: Settings
	_categories : Categories
	_images: Images
	_faces: Faces


	def __init__(self):
		ChangeObject.__init__(self)

	@staticmethod
	def default():
		data_store = DataStore()
		data_store._settings = Settings.default()
		data_store._identities = Identities.default(data_store)
		data_store._categories = Categories.default(data_store)
		data_store._images = Images.default(data_store)
		data_store._faces = Faces.default(data_store)
		return data_store

	@property
	def settings(self):
		return self._settings

	@property
	def identities(self):
		return self._identities

	@property
	def categories(self):
		return self._categories

	@property
	def images(self):
		return self._images

	@property
	def faces(self):
		return self._faces

	def serialize_json(self):
		return \
			{
				'ids': self._identities,
				'categories': self._categories,
				'settings': self._settings,
				'images': self._images,
				'faces': self._faces
			}

	@staticmethod
	def deserialize(data, ignore_version=False):
		item = DataStore()
		item.deserialize_data(data)
		return item

	def deserialize_data(self, data):
		self._identities = Identities.deserialize(data.get('ids', None), self)
		self._categories = Categories.deserialize(data.get('categories', None), self)
		self._settings = Settings.deserialize(data.get('settings', None))
		self._images = Images.deserialize(data.get('images', None), self)
		self._faces = Faces.deserialize(data.get('faces', None), self)