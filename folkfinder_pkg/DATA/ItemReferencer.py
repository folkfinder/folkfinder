# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from folkfinder_pkg.DATA.ChangeObject import ChangeObject
from folkfinder_pkg.DATA.ItemReference import ItemReference
from folkfinder_pkg.LOGIC.Events import ChangeEvent


class ItemReferencer(ChangeObject):
	def __init__(self, data_store):
		ChangeObject.__init__(self)
		self._data_store = data_store
		self._item_references = []
		self._refed_uids = {}

	def add_item_reference(self, item, reference_type):
		if item.uid in self._refed_uids:
			print("item already exists in list")
			return
		item_reference = ItemReference(self._data_store, reference_type, [item.uid, item.owner.uid])
		self.changed(ChangeEvent(self, ChangeEvent.BeforeObjectAdded, item_reference))
		self._item_references.append(item_reference)
		self._refed_uids[item.uid] = item_reference
		self.changed(ChangeEvent(self, ChangeEvent.ObjectAdded, item_reference))

	def remove_item_reference(self, item):
		item_reference = None
		for item_ref in self._item_references:
			if item_ref.data[0] == item.uid:
				item_reference = item_ref
				break
		if item_reference is None:
			return
		self.changed(ChangeEvent(self, ChangeEvent.BeforeObjectRemoved, item_reference))
		self._item_references.remove(item_reference)
		self._refed_uids.pop(item.uid)
		self.changed(ChangeEvent(self, ChangeEvent.ObjectRemoved, item_reference))

	@property
	def items(self):
		return self._item_references

	def get_referenced_items(self):
		items = []
		for item_reference in self._item_references:
			item = item_reference.get_item()
			if not item is None:
				items.append(item)
		return items

	def get_referenced_items_by_owner_id(self, owner_id):
		items = []
		for category_reference in self._item_references:
			item = category_reference.get_item()
			if not item is None:
				if item.owner.uid==owner_id:
					items.append(item)
		return items

	def serialize_json(self):
		return \
			{
				'item_refs': self._item_references
			}

	@staticmethod
	def deserialize(data):
		item = ItemReferencer()
		if not data is None:
			item.deserialize_data(data)
		return item

	def deserialize_data(self, data):
		if data is None:
			return
		for item in data['item_refs']:
			item_ref = ItemReference.deserialize(item, self._data_store)
			self._item_references.append(item_ref)
			self._refed_uids[item_ref.data[0]] = item_ref
