# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os

from folkfinder_pkg.DATA.ChangeObject import IdObject
from folkfinder_pkg.DATA.Face import Face
from folkfinder_pkg.DATA.ItemReferencer import ItemReferencer


class Image(ItemReferencer, IdObject):
	def __init__(self, data_store, path = "", hash = "", size = (0,0)):
		ItemReferencer.__init__(self, data_store)
		IdObject.__init__(self)
		self._path = path
		self._hash = hash
		self._size = size
		self._item_references = []
		self._name = os.path.basename(path)

	@property
	def owner(self):
		return self

	@property
	def name(self):
		return self._name

	@name.setter
	def name(self, value):
		self._name = value

	@property
	def hash(self):
		return self._hash

	@property
	def path(self):
		return self._path

	@property
	def file_format(self):
		return os.path.splitext(self._path)[1]

	@property
	def resolution(self):
		return str(self._size)

	@property
	def size(self):
		return self._size

	@property
	def identities(self):
		ids = []
		for face in self.faces:
			ids.append(face.identity.name)
		return ids

	@property
	def faces(self):
		faces = []
		for item in self.get_referenced_items():
			if type(item) is Face:
				faces.append(item)
		return faces

	def serialize_json(self):
		return \
			{
				'uid': IdObject.serialize_json(self),
				'ir': ItemReferencer.serialize_json(self),
				'hash': self._hash,
				'path': self._path,
				'name': self._name,
				'size': self._size
			}

	@staticmethod
	def deserialize(data, data_store):
		item = Image(data_store)
		item.deserialize_data(data)
		return item

	def deserialize_data(self, data):
		IdObject.deserialize_data(self, data['uid'])
		ItemReferencer.deserialize_data(self, data['ir'])
		self._hash = data.get('hash', "")
		self._path = data.get('path', "")
		self._name = data.get('name', os.path.basename(self._path))
		self._size = data.get('size')
