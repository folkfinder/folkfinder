# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from folkfinder_pkg.DATA.ChangeObject import ChangeObject
from folkfinder_pkg.DATA.Identity import Identity
from folkfinder_pkg.LOGIC.Events import ChangeEvent


class Identities(ChangeObject):
	def __init__(self, data_store):
		ChangeObject.__init__(self)
		self._data_store = data_store
		self._items = {}
		self._unknown_identity = None

	def add_identity(self, full_name, age):
		identity = Identity(self._data_store, full_name, age)
		self.changed(ChangeEvent(self, ChangeEvent.BeforeObjectAdded, identity))
		self._items[identity.uid] = identity
		self.changed(ChangeEvent(self, ChangeEvent.ObjectAdded, identity))
		return identity

	def remove_identity(self, identity):
		if identity.uid in self._items:
			self.changed(ChangeEvent(self, ChangeEvent.BeforeObjectRemoved, identity))
			self._items.pop(identity.uid)
			self.changed(ChangeEvent(self, ChangeEvent.ObjectRemoved, identity))

	def get_by_id(self, id):
		if id in self._items:
			return self._items[id]
		return None

	@property
	def items(self):
		return self._items.values()

	@property
	def unknown(self):
		return self._unknown_identity

	@unknown.setter
	def unknown(self, value):
		if self.unknown is None:
			self._unknown_identity = value

	def get_names(self):
		names = []
		for item in self.items:
			names.append(item.name)
		return names

	@staticmethod
	def default(data_store):
		identities = Identities(data_store)
		identities.unknown = identities.add_identity("Unknown", 0)
		return identities

	def serialize_json(self):
		return \
			{
				'items': list(self._items.values())
			}

	@staticmethod
	def deserialize(data, data_store):
		item = Identities(data_store)
		item.deserialize_data(data)
		return item

	def deserialize_data(self, data):
		if type(data['items']) is dict:
			for key in data['items']:
				item = data['items'][key]
				identity_item = Identity.deserialize(item, self._data_store)
				self._items[identity_item.uid] = identity_item
				if identity_item.name == "Unknown":
					self._unknown_identity = identity_item
		else:
			for item in data['items']:
				identity_item = Identity.deserialize(item, self._data_store)
				self._items[identity_item.uid] = identity_item
				if self._unknown_identity is None:
					self._unknown_identity = identity_item