# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from folkfinder_pkg.DATA.ChangeObject import ChangeObject
from folkfinder_pkg.DATA.Image import Image
from folkfinder_pkg.LOGIC.Events import ChangeEvent


class Images(ChangeObject):
	def __init__(self, data_store):
		ChangeObject.__init__(self)
		self._data_store = data_store
		self._items = {}
		self._hash_lookup = {}
		self._path_lookup = {}

	def get_image_by_hash(self, hash):
		if hash in self._hash_lookup:
			return self._hash_lookup[hash]
		return None

	def get_image_by_path(self, path):
		if path in self._path_lookup:
			return self._path_lookup[path]
		return None

	def get_image_by_uid(self, uid):
		if uid in self._items:
			return self._items[uid]
		return None

	def add_image(self, path, hash, size):
		image = Image(self._data_store, path, hash, size)
		self.changed(ChangeEvent(self, ChangeEvent.BeforeObjectAdded, image))
		self._items[image.uid] = image
		self._hash_lookup[image.hash] = image
		self._path_lookup[image.path] = image
		self.changed(ChangeEvent(self, ChangeEvent.ObjectAdded, image))
		return image

	@staticmethod
	def default(data_store):
		return Images(data_store)

	def serialize_json(self):
		return \
			{
				'items': list(self._items.values())
			}

	@staticmethod
	def deserialize(data, data_store):
		item = Images(data_store)
		if data != None:
			item.deserialize_data(data)
		return item

	def deserialize_data(self, data):
		for item in data['items']:
			image_item = Image.deserialize(item, self._data_store)
			self._items[image_item.uid] = image_item
			self._hash_lookup[image_item.hash] = image_item
			self._path_lookup[image_item.path] = image_item