# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import hashlib
import os

import imagesize

from folkfinder_pkg.DATA.CategoryItem import CategoryItem
from folkfinder_pkg.DATA.DataStore import DataStore
from folkfinder_pkg.DATA.Face import Face
from folkfinder_pkg.DATA.Identity import Identity
from folkfinder_pkg.DATA.Image import Image
from folkfinder_pkg.DATA.ItemReference import ItemReference
from folkfinder_pkg.LOGIC.Events import message_box, MessageType, MessageBoxButtons
from folkfinder_pkg.LOGIC.FaceDetectThread import FaceDetectThread


class Actions(object):
	def __init__(self, data_store, status_message_listener):
		self._data_store: DataStore = data_store
		self._status_message_listener = status_message_listener
		self._face_detector_thread:FaceDetectThread = None

	def add_identity(self, full_name, age):
		self._data_store.identities.add_identity(full_name, age)

	def remove_identity(self, identity):
		if identity.name == "Unknown":
			message_box("Information", "Unknown cannot be removed.", MessageType.Information, MessageBoxButtons.Ok)
			return
		faces = identity.get_faces()
		category_items = identity.get_category_items()
		unknown: Identity = self._data_store.identities.unknown
		for face in faces:
			face.set_identity(unknown)
			unknown.add_item_reference(face, ItemReference.FaceReference)
		for category_item in category_items:
			category_item.remove_item_reference(identity)

		self._data_store.identities.remove_identity(identity)

	def add_category(self, name, flags):
		self._data_store.categories.add_category(name, flags)

	def remove_category(self, category):
		for category_item in category.items:
			refed_items = category_item.get_referenced_items()
			for refed_item in refed_items:
				refed_item.remove_item_reference(category_item)
		self._data_store.categories.remove_category(category)

	def get_file_item(self, path):
		if os.path.isfile(path):
			hash = hashlib.md5(open(path, 'rb').read()).hexdigest()
			image_h = self._data_store.images.get_image_by_hash(hash)
			image_p = self._data_store.images.get_image_by_path(path)
			if image_h != image_p:
				pass  # Todo: solve conflict
			if image_h == None:
				size = imagesize.get(path)
				image_h = self._data_store.images.add_image(path, hash, size)
			return image_h
		return None

	def handle_category_item_change(self, data_item, category, value):
		selected_category_items = []
		if value['text'] != "":
			exists = False
			for category_item in category.items:
				if category_item.name.lower() == value['text'].lower():
					selected_category_items.append(category_item)
					exists = True
					break
			if not exists:
				category_item = category.add_category_item(value['text'])
				selected_category_items.append(category_item)

		for item in value['items']:
			if item['checked']:
				category_item = category.get_by_id(item['uid'])
				if not category_item in selected_category_items:
					selected_category_items.append(category_item)

		data_item_reference_type = 0
		if type(data_item) is Image:
			data_item_reference_type = ItemReference.ImageReference
		elif type(data_item) is CategoryItem:
			data_item_reference_type = ItemReference.CategoryReference
		elif type(data_item) is Identity:
			data_item_reference_type = ItemReference.IdentityReference

		old_list = data_item.get_referenced_items_by_owner_id(category.uid)
		for category_item in selected_category_items:
			if not category_item in old_list:
				data_item.add_item_reference(category_item, ItemReference.CategoryReference)
				category_item.add_item_reference(data_item, data_item_reference_type)
		for category_item in old_list:
			if not category_item in selected_category_items:
				data_item.remove_item_reference(category_item)
				category_item.remove_item_reference(data_item)
		return None

	def add_face(self, image, pos, size):
		face = self._data_store.faces.add_face(image, pos, size)
		image.add_item_reference(face, ItemReference.FaceReference)
		return face

	def tag_face(self, face, identity):
		if face is None or identity is None:
			return
		if not face.identity is None:
			face.identity.remove_item_reference(face)
		face.set_identity(identity)
		identity.add_face(face)

	def boxes_to_faces(self, boxes, image, size_threshold=2.0):
		faces = []
		(w, h) = image.size
		for box in boxes:
			size = [box[2] - box[0], box[3] - box[1]]
			pos = [box[0] + size[0] / 2, box[1] + size[1] / 2]
			size = [size[0] * 1.6, size[1] * 1.6]
			face_ratio = max(size[0] / w, size[1] / h)
			if pos[0] > w or pos[1] > h or face_ratio > size_threshold:
				continue
			face = Face(self._data_store, image, pos, size, 100)
			faces.append(face)
		return faces

	def on_face_detector_finished(self, result):
		for path in result:
			boxes = result[path]
			image = self.get_file_item(path)
			faces = self.boxes_to_faces(boxes, image)
			for face in faces:
				already_exists = False
				for existing_face in image.faces:
					if existing_face.inside(face.pos):
						already_exists = True
				if not already_exists:
					face.set_identity(self._data_store.identities.unknown)
					self._data_store.faces.add_face_object(face)
					self._data_store.identities.unknown.add_face(face)
					image.add_item_reference(face, ItemReference.FaceReference)
		self._face_detector_thread = None

	def on_message(self, message_object):
		if (self._status_message_listener!=None):
			self._status_message_listener(message_object['message'], message_object['progress'])

	def find_faces_in_all_pictures(self, root_path):
		if self._face_detector_thread is not None:
			return
		self._face_detector_thread = FaceDetectThread(root_path)
		self._face_detector_thread.finished_signal.connect(self.on_face_detector_finished)
		self._face_detector_thread.message_signal.connect(self.on_message)
		self._face_detector_thread.start()

	def remove_face(self, face: Face):
		face.identity.remove_item_reference(face)
		face.image.remove_item_reference(face)
		self._data_store.faces.remove_face(face)
		return None