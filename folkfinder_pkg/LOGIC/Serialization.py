# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import json
import zlib
import pickle
import appdirs
from pathlib import Path

from folkfinder_pkg.DATA import text_formats_endings, complex_handler
from folkfinder_pkg.DATA.DataStore import DataStore
from folkfinder_pkg.LOGIC.Upgrade import check_data_store_version

app_data_path = appdirs.user_config_dir("folkfinder") + "/" # os.path.expanduser("~") + "/.config/folkfinder/"
data_store_file_name = 'data_store.json'


def load_data_store(ignore_version=False):
	if os.path.isfile(app_data_path + data_store_file_name):
		data = read_json_data_from_disk(app_data_path + data_store_file_name)
		data_store = DataStore.deserialize(data, ignore_version)
		check_data_store_version(data_store)
		return data_store
	return DataStore.default()

def save_data_store(data_store: DataStore):
	if not os.path.isdir(app_data_path):
		os.makedirs(app_data_path)
	data = json.dumps(data_store, default=complex_handler)
	write_data_to_disk(app_data_path + data_store_file_name, data)

def load_data_store_old():
	if os.path.isfile(app_data_path + data_store_file_name):
		infile = open(app_data_path + data_store_file_name, 'rb')
		data_store = pickle.load(infile)
		infile.close()
		check_data_store_version(data_store)
		return data_store
	return DataStore.default()


def save_data_store_old(data_store):
	if not os.path.isdir(app_data_path):
		os.makedirs(app_data_path)
	outfile = open(app_data_path + data_store_file_name, 'wb')
	pickle.dump(data_store, outfile)
	outfile.close()


def read_json_data_from_disk(file_path):
	is_binary = True
	for ending in text_formats_endings:
		if ending in file_path.lower():
			is_binary = False
	if not is_binary:
		with open(file_path) as data_file:
			data = json.load(data_file)
	else:
		with open(file_path, "rb") as data_file:
			S = data_file.read()
			data_string = zlib.decompress(S)
			data = json.loads(data_string.decode("UTF-8"))
	return data


def write_data_to_disk(file_path, data):
	is_binary = True
	for ending in text_formats_endings:
		if ending in file_path.lower():
			is_binary = False
	if not is_binary:
		text_file = open(file_path, "w")
		text_file.write(data)
		text_file.close()
	else:
		code = zlib.compress(bytes(data, 'UTF-8'))
		text_file = open(file_path, "wb")
		text_file.write(code)
		text_file.close()