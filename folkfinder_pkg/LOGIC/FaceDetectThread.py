import os

from PyQt5.QtCore import QThread, pyqtSignal
from folkfinder_pkg.LOGIC.FaceDetector import FaceDetector
from folkfinder_pkg import image_file_endings

class FaceDetectThread(QThread):
	finished_signal = pyqtSignal('PyQt_PyObject')
	message_signal = pyqtSignal('PyQt_PyObject')

	def __init__(self, root_path):
		QThread.__init__(self)
		self._face_detector = FaceDetector()
		self._result = {}
		self._root_path = root_path

	@property
	def result(self):
		return self._result

	def find_faces_in_image(self, image_path):
		self._face_detector.load_image(image_path)
		boxes = self._face_detector.find_faces(300, 0.5)
		if max(self._face_detector.image_size[0], self._face_detector.image_size[1]) > 1200:
			small_boxes = self._face_detector.find_faces(1200, 0.5, 0.20)
			boxes.extend(small_boxes)
		return boxes

	def is_picture(self, file):
		for image_ending in image_file_endings:
			if file.endswith(image_ending):
				return True

	def get_all_picture_paths(self, path):
		files = []
		for r, d, f in os.walk(path):
			for file in f:
				if self.is_picture(file.lower()):
					files.append(os.path.join(r, file))
		return files

	def run(self):
		picture_paths = self.get_all_picture_paths(self._root_path)
		counter = 0
		for path in picture_paths:
			counter += 1
			message = {
				"message": "finding faces (" + str(counter) + "): " + path,
				"progress": 100 * counter / len(picture_paths)
			}
			self.message_signal.emit(message)
			boxes = self.find_faces_in_image(path)
			if len(boxes) > 0:
				self._result[path] = boxes
		self.finished_signal.emit(self._result)
