import pkgutil

import numpy
import numpy as np
import cv2


class FaceDetector():
	def __init__(self):
		self._prototext_file_path = "deploy.prototxt"
		self._caffe_model_file_path = "res10_300x300_ssd_iter_140000.caffemodel"
		self._nets = {}
		self._frame = None

	def _get_net(self, size):
		if not size in self._nets:
			prototext_file_data = pkgutil.get_data('folkfinder_pkg.dnnmodel', self._prototext_file_path)
			caffe_model_file_data = pkgutil.get_data('folkfinder_pkg.dnnmodel', self._caffe_model_file_path)
			self._nets[size] = cv2.dnn.readNetFromCaffe(prototext_file_data, caffe_model_file_data)
		return self._nets[size]

	def load_image(self, image_path):
		self._frame = cv2.imread(image_path)
		if self._frame is None:  # Wierd Windows unicode bug in opencv
			stream = open(image_path, "rb")
			bytes = bytearray(stream.read())
			numpyarray = numpy.asarray(bytes, dtype=numpy.uint8)
			self._frame = cv2.imdecode(numpyarray, cv2.IMREAD_COLOR)

	@property
	def image_size(self):
		if self._frame is None:
			return (0, 0)
		return self._frame.shape[:2]

	def find_faces(self, det_size = 300, confidense_thres_hold=0.5, size_threshold=2.0):
		boxes = []
		if self._frame is None:
			return boxes
		(h, w) = self._frame.shape[:2]
		blob = cv2.dnn.blobFromImage(cv2.resize(self._frame, (det_size, det_size)), 1.0, (det_size, det_size), (103.93, 116.77, 123.68))
		net = self._get_net(det_size)
		net.setInput(blob)
		detections = net.forward()
		for i in range(0, detections.shape[2]):
			confidence = detections[0, 0, i, 2]
			if confidence < confidense_thres_hold:
				continue
			box = detections[0, 0, i, 3:7]
			face_ratio = max(box[2]-box[0], box[3]-box[1])
			if face_ratio < size_threshold:
				box = detections[0, 0, i, 3:7]*np.array([w, h, w, h])
				boxes.append(box)
		return boxes