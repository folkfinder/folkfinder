# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from enum import Enum

message_box_listener = None


class MessageType(Enum):
	Information = 1
	Warning = 2
	Critical = 3
	Question = 4


class MessageBoxButtons(Enum):
	YesNoCancel = 1
	OkCancel = 2
	Ok = 3


def message_box(title, message, type = MessageType.Information, buttons = MessageBoxButtons.Ok):
	if message_box_listener != None:
		return message_box_listener(title, message, type, buttons)


class Event(object):
	def __init__(self, sender):
		self.sender = sender


class ChangeEvent(Event):
	ObjectAdded = 1
	ObjectRemoved = 2
	ValueChanged = 3
	BeforeObjectAdded = 4
	BeforeObjectRemoved = 5
	Cleared = 6
	Deleted = 7
	HiddenChanged = 8
	ObjectChanged = 9
	BeforeCleared = 10

	def __init__(self, sender, type, object):
		Event.__init__(self, sender)
		self.type = type
		self.object = object


class ValueChangeEvent(ChangeEvent):
	def __init__(self, sender, name, old_value, new_value):
		ChangeEvent.__init__(self, sender, ChangeEvent.ValueChanged, name)
		self.old_value = old_value
		self.new_value = new_value