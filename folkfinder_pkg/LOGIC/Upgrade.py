# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from folkfinder_pkg import __version__, __app_name__
from folkfinder_pkg.LOGIC.Events import message_box, MessageType, MessageBoxButtons


def check_data_store_version(data_store):
	needs_upgrade = False
	if data_store._settings._version != __version__:
		for i in range(len(__version__)):
			if __version__[i] > data_store._settings._version[i]:
				needs_upgrade = True
				break
			elif __version__[i] < data_store._settings._version[i]:
				message_box("Downgrade", "Data store is made with a newer version of " + __app_name__,
					MessageType.Information, MessageBoxButtons.Ok)
				print("needs downgrade")
	if needs_upgrade:
		handle_data_store_upgrade(data_store)


def handle_data_store_upgrade(data_store):
	print("upgrading data store from version: " + str(data_store._settings._version) + " to version " + str(__version__))
	print("nothing to do.")
	handle_settings_upgrade(data_store._settings)


def handle_settings_upgrade(settings):
	print("upgrading settings from version: " + str(settings._version) + " to version " + str(__version__))
	print("nothing to do.")
	settings._version = __version__