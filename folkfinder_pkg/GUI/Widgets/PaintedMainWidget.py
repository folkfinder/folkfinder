# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os

from PyQt5.QtCore import Qt, QRectF, QSize, QPointF, QRect, QEvent, QTimer, QPoint, QSizeF
from PyQt5.QtGui import QImageReader, QPixmap, QPaintEvent, QPainter, QPen, QPainterPath, QMouseEvent
from PyQt5.QtWidgets import QLabel, QSizePolicy, QStyle, QApplication, QWidget, QMenu

from folkfinder_pkg.GUI import gui_scale
from folkfinder_pkg.GUI.GuiState import GuiState


class PaintedMainWidget(QWidget):
	def __init__(self, main_window):
		QWidget.__init__(self, main_window)
		app = QApplication.instance()
		self._main_window = main_window
		self._state: GuiState = main_window.state # PaintedMainWidget.Viewing
		self._dragging = False
		self._sw = app.style().pixelMetric(QStyle.PM_ScrollBarExtent)
		self._pix_map = None
		self._gui_scale = gui_scale()
		self._current_scale = 1.0
		self._scale = 1.0
		self._factor = 1.0
		self._transform_speed = 0.2
		self._current_offset = QPointF()
		self._offset = QPointF()
		self.setMouseTracking(True)
		self.installEventFilter(self)
		self._mouse_position = None
		self._timer = QTimer()
		self._timer.timeout.connect(self.tick)
		self._timer.start(int(1000/60))
		self._drag_ref_position = None
		self._tag_size = QSizeF(200, 260)
		self._current_image = None
		self._current_face = None
		self._faces = []

	def tick(self):
		delta_scale = (self._scale-self._current_scale)*self._transform_speed
		delta_offset = (self._offset-self._current_offset)*self._transform_speed
		offset_length = delta_offset.manhattanLength()

		if abs(delta_scale) > 0.1 or offset_length > 1:
			self._current_offset += delta_offset
			self._current_scale += delta_scale
			self.update()
		else:
			pass

			#print("update")

	@property
	def current_image(self):
		return self._current_image

	def show_picture(self, image):
		if os.path.isfile(image.path):
			self._current_image = image
			self._faces = image.faces
			reader = QImageReader(image.path)
			reader.setAutoTransform(True)
			newImage = reader.read()
			pixMap = QPixmap()
			pixMap.convertFromImage(newImage)
			self._pix_map = pixMap
			self.resizeEvent(None)
			self._offset *= 0
			self._scale = 1
			self.update()

	def resizeEvent(self, QResizeEvent):
		if self._pix_map != None:
			image_size = self._pix_map.size()
			if image_size.width() != 0 and image_size.height() != 0:
				self._factor = min((self.width()-2) / image_size.width(), (self.height()-2) / image_size.height())

	def paintEvent(self, event: QPaintEvent):
		qp = QPainter()
		qp.begin(self)
		qp.setRenderHint(QPainter.SmoothPixmapTransform)
		qp.save()

		qp.translate(self._current_offset.x(), self._current_offset.y())
		qp.scale(self._current_scale, self._current_scale)

		if not self._pix_map is None:
			image_size = self._pix_map.size() * self._factor
			rect = QRect(0, 0, image_size.width(), image_size.height())
			qp.drawPixmap(rect, self._pix_map)

		qp.restore()

		if self._state.main_view_state == GuiState.Tagging:
			qp.setRenderHint(QPainter.HighQualityAntialiasing)
			posx = self._mouse_position.x() - self._tag_size.width()/2
			posy = self._mouse_position.y() - self._tag_size.height()/2
			rect = QRectF(posx, posy, self._tag_size.width(), self._tag_size.height())
			qp.setPen(QPen(Qt.black, 4 * self._gui_scale))
			qp.drawEllipse(rect)
			qp.setPen(QPen(Qt.white, 1.5 * self._gui_scale))
			qp.drawEllipse(rect)

		if self._state.show_faces or self._state.main_view_state == GuiState.Tagging:
			for face in self._faces:
				pen_scale = 1
				if face is self._current_face:
					pen_scale = 2
				self.draw_tag(qp, face.pos, face.size, face.identity.name + "\n" + str(face._confidence), pen_scale)
		qp.end()

	def draw_tag(self, qp: QPainter, pos, size, name, pen_scale=1.0):
		qp.setRenderHint(QPainter.HighQualityAntialiasing)

		sizex = size[0] * (self._current_scale * self._factor)
		sizey = size[1] * (self._current_scale * self._factor)

		posx = pos[0] * (self._current_scale * self._factor) + self._current_offset.x() - sizex/2
		posy = pos[1] * (self._current_scale * self._factor) + self._current_offset.y() - sizey/2
		rect = QRectF(posx, posy, sizex, sizey)

		qp.setPen(QPen(Qt.black, 4 * self._gui_scale * pen_scale))
		qp.drawEllipse(rect)
		qp.setPen(QPen(Qt.white, 1.5 * self._gui_scale * pen_scale))
		qp.drawEllipse(rect)

		posx = pos[0] * (self._current_scale * self._factor) + self._current_offset.x() - 100 * self._gui_scale
		posy = pos[1] * (self._current_scale * self._factor) + self._current_offset.y() + 15 * self._gui_scale + sizey/2
		rect = QRectF(posx, posy, 200 * self._gui_scale, 30 * self._gui_scale)
		qp.setPen(QPen(Qt.black))
		qp.drawText(rect, Qt.AlignHCenter, name)
		rect.adjust(-2* self._gui_scale,-2* self._gui_scale,-2* self._gui_scale,-2* self._gui_scale)
		qp.setPen(QPen(Qt.white))
		qp.drawText(rect, Qt.AlignHCenter, name)

	def check_hover(self, pos, size):
		sizex = size[0] * (self._current_scale * self._factor)
		sizey = size[1] * (self._current_scale * self._factor)

		posx = pos[0] * (self._current_scale * self._factor) + self._current_offset.x()
		posy = pos[1] * (self._current_scale * self._factor) + self._current_offset.y()
		if self._mouse_position is None:
			return False
		dist = max(abs(self._mouse_position.x()-posx),abs(self._mouse_position.y()-posy))
		if dist < min(sizex, sizey)/2:
			return True
		return False

	def eventFilter(self, obj, event):
		if event.type() == QEvent.KeyPress:
			if event.key() == Qt.Key_Delete:
				pass #self.on_delete()
				return True
			if event.key() == Qt.Key_Control:
				pass # self._states.multi_select = True
			if event.key() == Qt.Key_Escape:
				self.on_escape()
		if event.type() == QEvent.KeyRelease:
			if event.key() == Qt.Key_Control:
				pass # self._states.multi_select = False
		if event.type() == QEvent.GraphicsSceneMouseDoubleClick:
			print("double click")
		return False

	def on_escape(self):
		if self._state.main_view_state == GuiState.Tagging:
			self._state.main_view_state = GuiState.SingleView
		self.update()

	def mouseMoveEvent(self, q_mouse_event):
		position = q_mouse_event.pos()
		update_view = False
		face_found = False
		if self._mouse_position is not None:
			mouse_move = self._mouse_position - position
		if self._state.show_faces:
			for face in self._faces:
				if self.check_hover(face.pos, face.size):
					face_found = True
					if self._current_face != face:
						self._current_face = face
						update_view = True
						break
			if not face_found:
				self._current_face = None
				update_view = True

		self._mouse_position = position
		if self._dragging:
			self._current_offset -= mouse_move
			self._offset -= mouse_move
			self.update()
		if self._state.main_view_state == GuiState.Tagging or update_view:
			self.update()

	def wheelEvent(self, event):
		if self._mouse_position is not None:
			delta = event.angleDelta().y() / 400
			if self._scale + self._scale * (delta) > 0:
				self._scale += self._scale * (delta)
				middle = QPoint(self.width(), self.height())/2
				delta_pos = middle - self._mouse_position

				self._offset += delta_pos*delta-(middle-self._offset)*delta
				self.update()

	def mousePressEvent(self, q_mouse_event: QMouseEvent):
		self.setFocus()
		if q_mouse_event.button() == Qt.RightButton:
			if self._current_face is not None:
				self._main_window._selected_face = self._current_face
				self._main_window._remove_face_action.setEnabled(True)
				context_menu = QMenu(self)
				context_menu.addAction(self._main_window._remove_face_action)
				context_menu.exec(self.mapToGlobal( q_mouse_event.pos()))
				#self._main_window.on_remove_face()
		if q_mouse_event.button() == 1:
			if self._state.main_view_state == GuiState.SingleView:
				if self._current_face is None:
					self._dragging = True
				else:
					self._main_window.on_face_tagged(self._current_face)
			elif self._state.main_view_state == GuiState.Tagging:
				self._state.main_view_state = GuiState.SingleView
				image_pos = (self._mouse_position - self._offset) / (self._scale * self._factor)
				tag_size = self._tag_size / (self._scale * self._factor)
				self._main_window.on_image_face_tagged(self._current_image, image_pos, tag_size)
			self._drag_ref_position = self._mouse_position


	def mouseReleaseEvent(self, q_mouse_event):
		if q_mouse_event.button() == 4:
			return
		if q_mouse_event.button() == 1:
			self._dragging = False
			return

	def set_faces(self, faces):
		self._faces = faces
