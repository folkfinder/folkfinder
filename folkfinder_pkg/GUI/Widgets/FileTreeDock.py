# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from PyQt5.QtWidgets import QDockWidget, QTreeView, QFileSystemModel, QAbstractItemView

from folkfinder_pkg.GUI import tr, gui_scale
from folkfinder_pkg import image_file_endings


class FileTreeDock(QDockWidget):
	def __init__(self, main_window):
		QDockWidget.__init__(self, main_window)
		self._main_window = main_window
		self.setWindowTitle(tr("Folder view"))
		self.setObjectName("TreeViewDock")
		self.setMinimumWidth(int(250*gui_scale()))

		self._filters = []
		for ending in image_file_endings:
			self._filters.append("*" + ending)

		self._treeView = QTreeView(self)
		self.setWidget(self._treeView)


	def set_folder(self, path):
		self._model = QFileSystemModel()
		self._model.setNameFilters(self._filters)
		self._model.setNameFilterDisables(False)
		self._model.setRootPath(path)
		self._treeView.setModel(self._model)
		self._treeView.setRootIndex(self._model.index(path))
		self._treeView.setColumnWidth(0, int(250*gui_scale()))
		self._treeView.selectionModel().selectionChanged.connect(self.on_tree_selection_changed)
		self._treeView.setSelectionMode(QTreeView.ExtendedSelection)


	def on_tree_selection_changed(self, selection):
		paths = []
		selection = self._treeView.selectionModel().selection()

		for selrange in selection:
			for sel in selrange.indexes():
				path = self._model.filePath(sel)
				paths.append(path)
		self._main_window.on_file_tree_selection_changed(paths)
