# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os

from PyQt5.QtGui import QImageReader, QPixmap, QPaintEvent, QPainter
from PyQt5.QtWidgets import QLabel, QScrollArea, QSizePolicy, QStyle, QApplication


class MainWidget(QScrollArea):
	Viewing = 1
	Tagging = 2

	def __init__(self, main_window):
		QLabel.__init__(self, main_window)
		app = QApplication.instance()
		self._state = MainWidget.Viewing
		self._sw = app.style().pixelMetric(QStyle.PM_ScrollBarExtent)
		self._image_label = QLabel(self)
		self._image_label.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
		self._image_label.setScaledContents(True)
		self.setWidget(self._image_label)

	def show_picture(self, path):
		if os.path.isfile(path):
			reader = QImageReader(path)
			reader.setAutoTransform(True)
			newImage = reader.read()
			pixMap = QPixmap()
			pixMap.convertFromImage(newImage)
			self._image_label.setPixmap(pixMap)
			self._image_label.adjustSize()
			self.resizeEvent(None)

	def resizeEvent(self, QResizeEvent):
		if self._image_label.pixmap() != None:
			image_size = self._image_label.pixmap().size()
			if image_size.width() != 0 and image_size.height() != 0:

				factor = max((self.width()-2-self._sw) / image_size.width(), (self.height()-2-self._sw) / image_size.height())
				self._image_label.resize(self._image_label.pixmap().size() * factor)


	def set_state(self, value):
		self._state = value