# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from PyQt5.QtCore import Qt, QEvent
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QDialogButtonBox, QLineEdit, QScrollArea, QCheckBox, QDialog, \
	QPushButton, QSizePolicy


class SelectorWidget(QWidget):
	def __init__(self, parent):
		QWidget.__init__(self, parent)
		self._result = QDialog.Rejected
		self.setMinimumWidth(120)
		self.setMinimumHeight(250)
		self.setLayout(QVBoxLayout())
		self._line_edit = QLineEdit(self)
		self.layout().addWidget(self._line_edit)
		self._scroll_area = QScrollArea(self)
		self._scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
		self._scroll_area.setWidgetResizable(True)
		self.layout().addWidget(self._scroll_area)
		self._check_box_widget = QWidget(self._scroll_area)
		self._check_box_widget.setLayout(QVBoxLayout())
		self._check_box_widget.layout().setContentsMargins(0, 0, 0, 0)
		size_policy = QSizePolicy.Minimum
		self._check_box_widget.setSizePolicy(size_policy, size_policy)
		self._scroll_area.setWidget(self._check_box_widget)
		dialog_buttons = QDialogButtonBox(QDialogButtonBox.Cancel | QDialogButtonBox.Ok)
		dialog_buttons.accepted.connect(self.accept)
		dialog_buttons.rejected.connect(self.reject)
		self.layout().addWidget(dialog_buttons)
		self._line_edit.setFocus()
		self._check_boxes = []
		self.layout().setContentsMargins(0, 0, 0, 0)
		self._data = None
		self.setAutoFillBackground(True)
		self.installEventFilter(self)

	def eventFilter(self, QObject, evt: QEvent):
		if evt.type() == QEvent.KeyPress:
			if evt.key() == Qt.Key_Enter or evt.key() == Qt.Key_Return:
				self.accept()
				return True
		return False

	def set_data(self, data):
		self._data = data
		count = len(data['items'])
		self._check_boxes = []
		for i in range(count):
			check_box = QCheckBox(data['items'][i]['name'])
			self._check_box_widget.layout().addWidget(check_box)
			check_box.setChecked(data['items'][i]['checked'])
			self._check_boxes.append(check_box)
		self._line_edit.setFocus()

	@property
	def result(self):
		return self._result

	def accept(self):
		self._result = QDialog.Accepted
		data = self._data
		count = len(data['items'])
		for i in range(count):
			check_box = self._check_boxes[i]
			data['items'][i]['checked'] = check_box.isChecked()
		data['text'] = self._line_edit.text()
		self.close()

	def reject(self):
		self._result = QDialog.Rejected
		self.close()

