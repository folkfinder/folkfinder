# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from PyQt5.QtCore import QModelIndex, Qt, QAbstractItemModel, QRect
from PyQt5.QtGui import QPainter, QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QAbstractItemDelegate, QApplication, QStyle, QWidget, QComboBox, QStyledItemDelegate, \
	QStyleOptionViewItem, QCompleter, QDialog

from folkfinder_pkg.GUI.Widgets.SelectorWidget import SelectorWidget

__author__ = 'mamj'


class PropertiesTableDelegate(QStyledItemDelegate):
	CheckedComboBox = 1

	def __init__(self, parent):
		QStyledItemDelegate.__init__(self, parent)

	def paint(self, painter: QPainter, item: QStyleOptionViewItem, index: QModelIndex):
		QStyledItemDelegate.paint(self, painter, item, index)

	def createEditor(self, parent: QWidget, option: QStyleOptionViewItem, index: QModelIndex):
		data = index.model().data(index, Qt.EditRole)
		if type(data) is dict:
			if data['delegate_type'] == PropertiesTableDelegate.CheckedComboBox:
				return self.createSelectorEditor(parent, data)
				#return self.createCheckedComboBoxEditor(parent, data)
		return QStyledItemDelegate.createEditor(self, parent, option, index)

	def createCheckedComboBoxEditor(self, parent, data):
		editor = QComboBox(parent)
		count = len(data['items'])
		model = QStandardItemModel(count, 1)
		editor.setEditable(True)
		for i in range(count):
			item = QStandardItem(data['items'][i]['name'])
			item.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled )#| Qt.ItemIsSelectable)
			if data['items'][i]['checked']:
				item.setData(Qt.Checked, Qt.CheckStateRole)
			else:
				item.setData(Qt.Unchecked, Qt.CheckStateRole)
			model.setItem(i, 0, item)
		editor.setModel(model)
		editor.setCurrentText("")
		editor.data = data
		return editor

	def createSelectorEditor(self, parent, data):
		editor = SelectorWidget(parent)
		editor.data = data
		return editor

	def setEditorData(self, editor, index: QModelIndex):
		if type(editor) is QComboBox:
			value = index.model().data(index, Qt.DisplayRole)
			model = index.model()
			options = model.data(index, Qt.EditRole)

			#editor.setCurrentIndex(options.index(value))
		elif type(editor) is SelectorWidget:
			editor.set_data(editor.data)
		else:
			QStyledItemDelegate.setEditorData(self, editor, index)

	def setModelData(self, editor: QComboBox, model: QAbstractItemModel, index: QModelIndex):
		if type(editor) is QComboBox and hasattr(editor, 'data'):
			value = editor.currentIndex()
			editor.data['text'] = editor.currentText()
			count = len(editor.data['items'])
			for i in range(count):
				check_box_item = editor.model().item(i)
				checked = check_box_item.checkState() == Qt.Checked
				editor.data['items'][i]['checked'] = checked
			model.setData(index, editor.data, Qt.EditRole)
			editor.showPopup()
		elif type(editor) is SelectorWidget:
			if editor.result == QDialog.Accepted:
				model.setData(index, editor.data, Qt.EditRole)
		else:
			QStyledItemDelegate.setModelData(self, editor, model, index)

	def updateEditorGeometry(self, editor: QWidget, option: QStyleOptionViewItem, QModelIndex):
		editor.setGeometry(option.rect)
