# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from PyQt5.QtWidgets import QDockWidget, QTableView

from folkfinder_pkg.GUI import tr, gui_scale
from folkfinder_pkg.GUI.Models.PropertiesModel import PropertiesModel
from folkfinder_pkg.GUI.Widgets.PropertiesTableDelegate import PropertiesTableDelegate


class PropertiesDock(QDockWidget):
	def __init__(self, main_window):
		QDockWidget.__init__(self, main_window)
		self._main_window = main_window
		self.setWindowTitle(tr("Properties"))
		self.setObjectName("PropertiesDock")
		self.setMinimumWidth(int(250 * gui_scale()))
		self._table_view = QTableView(self)
		self.setWidget(self._table_view)
		self._model = PropertiesModel(main_window.data_store, main_window.actions)
		self._table_view.setModel(self._model)
		self._table_view.horizontalHeader().hide()
		self._table_view.verticalHeader().hide()
		self._table_view.setColumnWidth(0, int(100 * gui_scale()))
		self._table_view.setColumnWidth(1, int(150 * gui_scale()))
		self._table_view.setEditTriggers(QTableView.AllEditTriggers)

		self._table_view.setItemDelegate(PropertiesTableDelegate(self))

	def on_data_items_selected(self, data_items):
		self._model.set_data_items(data_items)
		self._table_view.resizeRowsToContents()

	def resizeEvent(self, QResizeEvent):
		colw = self._table_view.columnWidth(0)
		self._table_view.setColumnWidth(1, self.width()-2  - colw)
		#factor = min((self.width()-2) / image_size.width(), (self.height()-2) / image_size.height())
		#self._image_label.resize(self._image_label.pixmap().size() * factor)
		self._table_view.resizeRowsToContents()