# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from PyQt5.QtWidgets import QDockWidget, QTreeView
from folkfinder_pkg.GUI import tr, gui_scale
from folkfinder_pkg.GUI.Models.ProjectModel import ProjectModel


class ProjectTreeDock(QDockWidget):
	def __init__(self, main_window):
		QDockWidget.__init__(self, main_window)
		self._main_window = main_window
		self.setWindowTitle(tr("Library"))
		self.setObjectName("ProjectViewDock")
		self.setMinimumWidth(int(250*gui_scale()))


		self._treeView = QTreeView(self)
		self.setWidget(self._treeView)
		self._model = ProjectModel(main_window._data_store)
		self._treeView.setModel(self._model)
		self._treeView.setRootIndex(self._model.index(0, 0))
		self._treeView.setHeaderHidden(True)
		self._treeView.selectionModel().selectionChanged.connect(self.on_tree_selection_changed)


	def on_tree_selection_changed(self, selection):
		for selrange in selection:
			for sel in selrange.indexes():
				item = sel.internalPointer()
				self._main_window.on_project_tree_selection_changed(item.data)
				return