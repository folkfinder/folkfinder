# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QStyle

from folkfinder_pkg.DATA.Categories import Categories
from folkfinder_pkg.DATA.Category import Category
from folkfinder_pkg.DATA.CategoryItem import CategoryItem
from folkfinder_pkg.DATA.DataStore import DataStore
from folkfinder_pkg.DATA.Face import Face
from folkfinder_pkg.DATA.Identities import Identities
from folkfinder_pkg.DATA.Identity import Identity
from folkfinder_pkg.DATA.Image import Image
from folkfinder_pkg.DATA.ItemReference import ItemReference
from folkfinder_pkg.DATA.Settings import Settings
from folkfinder_pkg.GUI import tr, get_icon
from folkfinder_pkg.LOGIC.Events import ChangeEvent


class ProjectModelItem(QObject):
	def __init__(self, parent, data, model, name=None, icon=None):
		if parent is not None and parent.level > 8:
			return
		QObject.__init__(self, parent)
		self._model = model
		self._level = 0
		if parent is not None:
			self._level = parent.level+1
		self._name = None
		if name is not None:
			self._name = tr(name, 'model')

		if icon is None:
			self._icon = self.get_icon_based_on_type(data)
		else:
			self._icon = icon
		if data is not None:
			if type(data) is ItemReference:
				data = data.get_item()
			data.add_change_handler(self.data_changed)
		self._data = data
		self._populated = False

	@property
	def level(self):
		return self._level

	@property
	def name(self):
		if self._name is not None:
			return self._name
		if self._data is not None:
			if hasattr(self._data, "name"):
				return self._data.name
		return self._name

	@name.setter
	def name(self, value):
		if hasattr(self._data, "name"):
			self._data.name = value

	@property
	def data(self):
		return self._data

	@property
	def icon(self):
		return self._icon

	@property
	def populated(self):
		return self._populated

	def data_changed(self, event: ChangeEvent):
		if event.type == ChangeEvent.BeforeObjectAdded:
			self._model.on_before_object_added(self, event.object)
		elif event.type == ChangeEvent.ObjectAdded:
			self._model.on_object_added(self, event.object)
		elif event.type == ChangeEvent.BeforeObjectRemoved:
			self._model.on_before_object_removed(self, event)
		elif event.type == ChangeEvent.ObjectRemoved:
			event.object.remove_change_handler(self.data_changed)
			removed_object = event.object
			if type(event.object) is ItemReference:
				removed_object = event.object.get_item()
			for child in self.children():
				if child.data == removed_object:
					self._model.on_object_removed(self, child)
					return
				for child_child in child.children():
					if child_child.data == removed_object:
						self._model.on_object_removed(self, child_child)
						return
		elif event.type == ChangeEvent.Deleted:
			self._data.remove_change_handler(self.data_changed)
			self._data = None
			self._model.on_object_deleted(self)
		elif event.type == ChangeEvent.ValueChanged:
			self._model.on_object_changed(self)

	def populate(self):
		if self._populated:
			return
		self._populated = True
		if type(self._data) is DataStore:
			new_item = ProjectModelItem(self, self._data.identities, self._model, "Folk")
			new_item = ProjectModelItem(self, self._data.categories, self._model, "Categories")
			new_item = ProjectModelItem(self, self._data.settings, self._model, "Settings")
		elif type(self._data) is Identities:
			for identity in self._data.items:
				new_item = ProjectModelItem(self, identity, self._model)
		elif type(self._data) is Categories:
			for category in self._data.items:
				new_item = ProjectModelItem(self, category, self._model)
		elif type(self._data) is Category:
			for category_item in self._data.items:
				new_item = ProjectModelItem(self, category_item, self._model)
		elif type(self._data) is CategoryItem:
			for item in self._data.items:
				if item.get_item() == self.parent().data:
					continue
				new_item = ProjectModelItem(self, item, self._model)
		elif type(self._data) is Identity:
			for item in self._data.items:
				if item.get_item() == self.parent().data:
					continue
				new_item = ProjectModelItem(self, item, self._model)
		elif type(self._data) is Image:
			for item in self._data.items:
				data_item = item.get_item()
				if data_item == self.parent().data:
					continue
				new_item = ProjectModelItem(self, item, self._model)
		elif type(self._data) is Face:
			if self._data.identity is not self.parent().data:
				new_item = ProjectModelItem(self, self._data.identity, self._model)



	@staticmethod
	def get_icon_based_on_type(obj):
		if type(obj) is Identity:
			return get_icon("id")
		if type(obj) is Identities:
			return get_icon("folder")
		if type(obj) is Settings:
			return get_icon("config")
		if type(obj) is Category:
			return get_icon("category")
		if type(obj) is CategoryItem:
			return get_icon("folder")
		if type(obj) is Categories:
			return get_icon("folder")
		if type(obj) is Image:
			return get_icon("image")
		if type(obj) is Face:
			return get_icon("tag_id")
		if type(obj) is ItemReference:
			return ProjectModelItem.get_icon_based_on_type(obj.get_item())
		return get_icon(QStyle.SP_DirOpenIcon)
