# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from enum import Enum

from PyQt5.QtCore import QObject, Qt

import folkfinder_pkg.LOGIC.Actions as actions
from folkfinder_pkg.DATA.DataStore import DataStore
from folkfinder_pkg.DATA.Identity import Identity
from folkfinder_pkg.DATA.Image import Image
from folkfinder_pkg.GUI.Widgets.PropertiesTableDelegate import PropertiesTableDelegate
from folkfinder_pkg.LOGIC.Events import ChangeEvent


class PropertyType(Enum):
	IdentityList = 1
	CategoryList = 2

lookup_tables = {
	'Settings': [
		['Version', 'version']
	],
	'Identity': [
		['Name', 'name'],
		['Age', 'age']
	],
	'Category': [
		['Name', 'name'],
		['Image', 'is_image_category'],
		['Person', 'is_identity_category'],
		['Location', 'is_location_category'],
		['Chronology', 'is_chronology_category'],
	],
	'CategoryItem': [
		['Name', 'name'],
		['Category', 'category_name']
	],
	'Image': [
		['Name', 'name'],
		['File Format', 'file_format'],
		['File Path', 'path'],
		['Resolution', 'resolution'],
		['Folk', 'identities'], # { 'type': PropertyType.IdentityList }],
	],
	'Face': [
		['Name', 'name'],
		['Position', 'pos'],
	]
}

class PropertiesModelItem(QObject):
	def __init__(self, model, data_items, data_store):
		QObject.__init__(self)
		self._model = model
		self._data_store: DataStore = data_store
		self._data_items = data_items
		self._lookup_table = []
		self._lookup_table_data = {}
		self._common_type = None
		self.get_lookup_table()

	def get_lookup_table(self):
		lookup_table = []
		if self._data_items[0].__class__.__name__ in lookup_tables:
			lookup_table = lookup_tables[self._data_items[0].__class__.__name__]
			self._common_type = type(self._data_items[0])
		for data_item in self._data_items:
			if type(data_item) is not type(self._data_items[0]):
				lookup_table = []
				self._common_type = None

		lookup_table_data = {}
		for table_item in lookup_table:
			for data_item in self._data_items:
				data = getattr(data_item, table_item[1])
				if table_item[1] in lookup_table_data:
						if lookup_table_data[table_item[1]] != str(data):
							lookup_table_data[table_item[1]] = "..."
				else:
					lookup_table_data[table_item[1]] = str(data)

		if self._common_type is Image:
			self._data_store.categories.image_categories

		self._lookup_table = lookup_table
		self._lookup_table_data = lookup_table_data

	@property
	def column_count(self):
		return 2

	@property
	def row_count(self):
		count = 1+len(self._lookup_table)
		if self._common_type is Image:
			return count + len(self._data_store.categories.image_categories)
		elif self._common_type == Identity:
			return count + len(self._data_store.categories.identity_categories)
		return count

	def data(self, row, col, role):
		if role == Qt.DisplayRole or role == Qt.EditRole:
			if row-1 >= len(self._lookup_table):
				if self._common_type == Image:
					if col == 0:
						category = self._data_store.categories.image_categories[row-1-len(self._lookup_table)]
						return category.name
					else:
						category = self._data_store.categories.image_categories[row - 1 - len(self._lookup_table)]
						if role == Qt.EditRole:
							return self.create_checked_combo_box_data(category)
						names = []
						for item in self._data_items[0].get_referenced_items():
							if item.owner == category:
								names.append(item.name)
						return str(names)
				elif self._common_type == Identity:
					if col == 0:
						category = self._data_store.categories.identity_categories[row-1-len(self._lookup_table)]
						return category.name
					else:
						category = self._data_store.categories.identity_categories[row - 1 - len(self._lookup_table)]
						if role == Qt.EditRole:
							return self.create_checked_combo_box_data(category)
						names = []
						for item in self._data_items[0].get_referenced_items():
							if item.owner == category:
								names.append(item.name)
						return str(names)
				return None
			if col == 0:
				if row == 0:
					return "Type"
				else:
					return self._lookup_table[row-1][0]
			else:

				if row == 0:
					return self._data_items[0].__class__.__name__
				else:
					if type(self._lookup_table[row - 1][1]) == str:
						data = getattr(self._data_items[0], self._lookup_table[row - 1][1])
						if role == Qt.DisplayRole:
							return self._lookup_table_data[self._lookup_table[row - 1][1]]
						else:
							return data
		return None

	def create_checked_combo_box_data(self, category):
		data_items = []
		data = {
			'delegate_type': PropertiesTableDelegate.CheckedComboBox,
			'items': data_items
		}
		selected_items = []
		for item in self._data_items[0].get_referenced_items():
			if item.owner == category:
				selected_items.append(item)

		for category_item in category.items:
			checked = category_item in selected_items
			data_items.append({ 'checked': checked, 'name': category_item.name, 'uid': category_item.uid })
		return data

	def set_data(self, row, col, role, value):
		if role == Qt.EditRole:
			if row - 1 >= len(self._lookup_table):
				if self._common_type == Image:
					for data_item in self._data_items:
						category = self._data_store.categories.image_categories[row - 1 - len(self._lookup_table)]
						self._model.actions.handle_category_item_change(data_item, category, value)
					self.get_lookup_table() # updating the contents of the table
					return True
				elif self._common_type == Identity:
					for data_item in self._data_items:
						category = self._data_store.categories.identity_categories[row - 1 - len(self._lookup_table)]
						self._model.actions.handle_category_item_change(data_item, category, value)
					self.get_lookup_table() # updating the contents of the table
					return True
			setattr(self._data_items[0], self._lookup_table[row - 1][1], value)
			self.get_lookup_table()
			return True
		return False

	def is_editable(self, row):
		if row - 1 >= len(self._lookup_table):
			return True
		if len(lookup_tables) <= row-1 or row == 0:
			return False
		try:
			attr_name = self._lookup_table[row-1][1]
		except (KeyError, IndexError) as e:
			print(e)
			return False
		class_attribute = getattr(type(self._data_items[0]), attr_name, None)
		if isinstance(class_attribute, property):
			if class_attribute.fset is not None:
				return True
		return False

	def data_changed(self, event: ChangeEvent):
		if event.type == ChangeEvent.Deleted:
			self._data.remove_change_handler(self.data_changed)
			self._data = None
			self._model.on_object_deleted(self)
		elif event.type == ChangeEvent.ValueChanged:
			self._model.on_object_changed(self)


class PropertiesModelItemOld(QObject):
	def __init__(self, model, data_item, data_store):
		QObject.__init__(self)
		self._model = model
		self._data_store: DataStore = data_store
		self._data_item = data_item
		self._lookup_table = []
		if self._data_item.__class__.__name__ in lookup_tables:
			self._lookup_table = lookup_tables[self._data_item.__class__.__name__]
		#if data_items is not None:
		#	data_items.add_change_handler(self.data_changed)

	@property
	def column_count(self):
		return 2

	@property
	def row_count(self):
		count = 1+len(self._lookup_table)
		if type(self._data_item) is Image:
			return count + len(self._data_store.categories.image_categories)
		return count

	def data(self, row, col, role):
		if role == Qt.DisplayRole or role == Qt.EditRole:
			if row-1 >= len(self._lookup_table):
				if type(self._data_item) == Image:
					if col == 0:
						category = self._data_store.categories.image_categories[row-1-len(self._lookup_table)]
						return category.name
					else:
						category = self._data_store.categories.image_categories[row - 1 - len(self._lookup_table)]
						if role == Qt.EditRole:
							return self.create_checked_combo_box_data(category)
						names = []
						for item in self._data_item.get_referenced_items():
							if item.owner == category:
								names.append(item.name)
						return str(names)
						#return "-"
				return None
			if col == 0:
				if row == 0:
					return "Type"
				else:
					return self._lookup_table[row-1][0]
			else:

				if row == 0:
					return self._data_item.__class__.__name__
				else:
					if type(self._lookup_table[row - 1][1]) == str:
						data = getattr(self._data_item, self._lookup_table[row-1][1])
						if role == Qt.DisplayRole:
							return str(data)
						else:
							return data

		return None

	def create_checked_combo_box_data(self, category):
		data_items = []
		data = {
			'delegate_type': PropertiesTableDelegate.CheckedComboBox,
			'items': data_items
		}
		selected_items = []
		for item in self._data_item.get_referenced_items():
			if item.owner == category:
				selected_items.append(item)
		#selected_items = self._data_item.get_category_items(category)
		for category_item in category.items:
			checked = category_item in selected_items
			data_items.append({ 'checked': checked, 'name': category_item.name, 'uid': category_item.uid })
		return data

	def set_data(self, row, col, role, value):
		if role == Qt.EditRole:
			#try:
			if row - 1 >= len(self._lookup_table):
				if type(self._data_item) == Image:
					category = self._data_store.categories.image_categories[row - 1 - len(self._lookup_table)]
					actions.handle_category_item_change(self._data_store, self._data_item, category, value)
					return True
			setattr(self._data_item, self._lookup_table[row-1][1], value)
			return True
			#except Exception as e:
			#	print(str(e))
		return False

	def is_editable(self, row):
		if row - 1 >= len(self._lookup_table):
			return True
		if len(lookup_tables) <= row-1 or row == 0:
			return False
		try:
			attr_name = self._lookup_table[row-1][1]
		except (KeyError, IndexError) as e:
			print(e)
			return False
		class_attribute = getattr(type(self._data_item), attr_name, None)
		if isinstance(class_attribute, property):
			if class_attribute.fset is not None:
				return True
		return False

	def data_changed(self, event: ChangeEvent):
		if event.type == ChangeEvent.Deleted:
			self._data.remove_change_handler(self.data_changed)
			self._data = None
			self._model.on_object_deleted(self)
		elif event.type == ChangeEvent.ValueChanged:
			self._model.on_object_changed(self)