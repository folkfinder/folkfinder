# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from PyQt5.QtCore import QAbstractItemModel, QModelIndex, Qt

from folkfinder_pkg.DATA.DataStore import DataStore
from folkfinder_pkg.GUI.Models.ProjectModelItem import ProjectModelItem


class ProjectModel(QAbstractItemModel):
	def __init__(self, data_store: DataStore):
		QAbstractItemModel.__init__(self)
		self._data_store = data_store
		self._root_item = ProjectModelItem(None, data_store, self, name="Project", icon=None)

	def columnCount(self, parent: QModelIndex=None, *args, **kwargs):
		return 1

	def rowCount(self, parent: QModelIndex=None, *args, **kwargs):
		if parent is None:
			return 1
		model_item = parent.internalPointer()
		if model_item is None:
			return 1
		else:
			if not model_item.populated:
				model_item.populate()
			return len(model_item.children())

	def index(self, row, col, parent: QModelIndex = None, *args, **kwargs):
		if parent is None:
			return self.createIndex(row, col, self._root_item)
		parent_model_item = parent.internalPointer()
		if parent_model_item is None:
			return self.createIndex(row, col, self._root_item)
		else:
			try:
				if not parent_model_item.populated:
					parent_model_item.populate()
				model_item = parent_model_item.children()[row]
				return self.createIndex(row, 0, model_item)
			except IndexError as e:
				pass
		return QModelIndex()

	def parent(self, child: QModelIndex) -> QModelIndex:
		if child.isValid():
			model_item = child.internalPointer()
			if model_item is not None and model_item is not self._root_item:
				parent_item = model_item.parent()
				if parent_item is not None:
					elder_item = parent_item.parent()
					if elder_item is not None:
						row = elder_item.children().index(parent_item)
					else:
						row = 0
					return self.createIndex(row, 0, parent_item)
		return QModelIndex()

	def data(self, index: QModelIndex, role: int = ...):
		model_item = index.internalPointer()
		if role == Qt.DisplayRole or role == Qt.EditRole:
			return model_item.name
		elif role == Qt.DecorationRole:
			return model_item.icon
		return None

	def setData(self, model_index: QModelIndex, value, role: int = ...) -> bool:
		if role == Qt.EditRole:
			model_item: ProjectModelItem = model_index.internalPointer()
			model_item.name = str(value)
		return False

	def flags(self, model_index: QModelIndex):
		default_flags = Qt.ItemIsEnabled | Qt.ItemIsSelectable
		model_item: ProjectModelItem = model_index.internalPointer()

		class_attribute = getattr(type(model_item.data), 'name', None)
		if isinstance(class_attribute, property):
			if class_attribute.fset is not None:
				default_flags |= Qt.ItemIsEditable
			else:
				pass # print("not editable")
		return default_flags

	def on_before_object_added(self, parent_item, object):
		self.layoutAboutToBeChanged.emit()

	def on_object_added(self, parent_item, object):
		if parent_item.populated:
			item = ProjectModelItem(parent_item, object, self)
		self.layoutChanged.emit()

	def on_before_object_removed(self, parent_item, event):
		self.layoutAboutToBeChanged.emit()

	def on_object_removed(self, parent_item, item):
		self.remove_item(item)
		self.layoutChanged.emit()

	def on_object_changed(self, item):
		self.layoutAboutToBeChanged.emit()
		self.layoutChanged.emit()

	def remove_item(self, item):
		parent = item.parent()
		if parent:
			if parent.parent():
				parent_row = parent.parent().children().index(parent)
			else:
				parent_row = 0
			index = parent.children().index(item)
			if index != -1:
				parent_model_index = self.createIndex(parent_row, 0, parent)
				self.beginRemoveRows(parent_model_index, index, index)
				item.setParent(None)
				self.endRemoveRows()