# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from PyQt5.QtCore import QAbstractTableModel, QModelIndex, Qt
from folkfinder_pkg.GUI.Models.PropertiesModelItem import PropertiesModelItem


class PropertiesModel(QAbstractTableModel):
	def __init__(self, data_store, actions):
		QAbstractTableModel.__init__(self)
		self._actions = actions
		self._data_store = data_store
		self._current_properties_model_item = None

	@property
	def actions(self):
		return self._actions

	def set_data_items(self, data_items):
		self.layoutAboutToBeChanged.emit()
		self._current_properties_model_item = PropertiesModelItem(self, data_items, self._data_store)
		self.layoutChanged.emit()

	def rowCount(self, parent: QModelIndex = ...) -> int:
		if self._current_properties_model_item != None:
			return self._current_properties_model_item.row_count
		return 2

	def columnCount(self, parent: QModelIndex = ...) -> int:
		if self._current_properties_model_item != None:
			return self._current_properties_model_item.column_count
		return 2

	def data(self, index: QModelIndex, role: int = ...):
		if self._current_properties_model_item != None:
			row = index.row()
			col = index.column()
			return self._current_properties_model_item.data(row, col, role)
		if role == Qt.DisplayRole:
			return "data"
		return None

	def setData(self, index: QModelIndex, value, role: int = ...) -> bool:
		if self._current_properties_model_item != None:
			row = index.row()
			col = index.column()
			return self._current_properties_model_item.set_data(row, col, role, value)
		return False

	def headerData(self, section: int, orientation: Qt.Orientation, role: int = ...):
		return None

	def flags(self, model_index: QModelIndex):
		default_flags = Qt.ItemIsSelectable | Qt.ItemIsEnabled
		col = model_index.column()
		if col > 0:
			if self._current_properties_model_item != None:
				row = model_index.row()
				if self._current_properties_model_item.is_editable(row):
					default_flags |= Qt.ItemIsEditable
		return default_flags

	def on_object_changed(self, item):
		self.layoutAboutToBeChanged.emit()
		self.layoutChanged.emit()