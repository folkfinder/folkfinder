# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from PyQt5 import QtGui
from PyQt5.Qt import Qt
from PyQt5.QtCore import QPointF
from PyQt5.QtGui import QPen
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout, QLabel, QGridLayout, QSizePolicy, QSpacerItem
from folkfinder_pkg.GUI import gui_scale, get_stylesheet


__author__ = 'mamj'


class RibbonPane(QWidget):
	def __init__(self, parent, name):
		QWidget.__init__(self, parent)
		self.setStyleSheet(get_stylesheet("ribbonPane"))
		hlayout = QHBoxLayout()
		hlayout.setSpacing(0)
		hlayout.setContentsMargins(0, 0, 0, 0)
		self.setLayout(hlayout)
		vWidget = QWidget(self)
		hlayout.addWidget(vWidget)
		hlayout.addWidget(RibbonSeparator(self))
		vlayout = QVBoxLayout()
		vlayout.setSpacing(0)
		vlayout.setContentsMargins(0, 0, 0, 0)
		vWidget.setLayout(vlayout)
		label = QLabel(name)
		label.setAlignment(Qt.AlignCenter)
		label.setStyleSheet("color:rgba(0, 0, 0, 50%);")
		contentWidget = QWidget(self)
		vlayout.addWidget(contentWidget)
		vlayout.addWidget(label)
		contentLayout = QHBoxLayout()
		contentLayout.setAlignment(Qt.AlignLeft)
		contentLayout.setSpacing(0)
		contentLayout.setContentsMargins(0, 0, 0, 0)
		self.contentLayout = contentLayout
		contentWidget.setLayout(contentLayout)

	def add_ribbon_widget(self, widget):
		self.contentLayout.addWidget(widget, 0, Qt.AlignTop)
		return widget

	def add_grid_widget(self, width):
		widget = QWidget()
		# widget.setMaximumWidth(width)
		gridLayout = QGridLayout()
		widget.setLayout(gridLayout)
		gridLayout.setSpacing(4)
		gridLayout.setContentsMargins(4, 4, 4, 4)
		self.contentLayout.addWidget(widget)
		gridLayout.setAlignment(Qt.AlignTop | Qt.AlignLeft)
		return gridLayout


class RibbonSeparator(QWidget):
	def __init__(self, parent):
		QWidget.__init__(self, parent)
		self.setMinimumHeight(int(gui_scale() * 80))
		self.setMaximumHeight(int(gui_scale() * 80))
		self.setMinimumWidth(5)
		self.setMaximumWidth(5)
		self.setLayout(QHBoxLayout())

	def paintEvent(self, event):
		qp = QtGui.QPainter()
		qp.begin(self)
		qp.setPen(QPen(QtGui.QColor(0, 0, 0, 50)))
		qp.drawLine(QPointF(2, 0), QPointF(2, self.height()))
		qp.end()
