# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os

from PyQt5.QtCore import Qt, QEvent, QSettings, QPoint, QSize
from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QMainWindow, QProgressBar, QToolBar, QAction, QStyle, QFileDialog, QInputDialog, \
	QMessageBox

from folkfinder_pkg.DATA.Category import Category
from folkfinder_pkg.DATA.Face import Face

from folkfinder_pkg.DATA.Identity import Identity
from folkfinder_pkg.DATA.Image import Image
from folkfinder_pkg.DATA.ItemReference import ItemReference
from folkfinder_pkg.GUI.GuiState import GuiState
from folkfinder_pkg.GUI.Ribbon.RibbonButton import RibbonButton
from folkfinder_pkg.GUI.Widgets.FileTreeDock import FileTreeDock
from folkfinder_pkg.GUI import gui_scale, get_icon, is_dark_theme, get_stylesheet, tr
from folkfinder_pkg.GUI.Ribbon.RibbonWidget import RibbonWidget
from folkfinder_pkg.GUI.Widgets.PaintedMainWidget import PaintedMainWidget
from folkfinder_pkg.GUI.Widgets.ProjectTreeDock import ProjectTreeDock
from folkfinder_pkg.LOGIC.Actions import Actions # import add_identity, remove_identity, add_category
from folkfinder_pkg.GUI.Widgets.PropertiesDock import PropertiesDock
from folkfinder_pkg.LOGIC.Serialization import load_data_store, save_data_store
from folkfinder_pkg import __app_name__, __version__


class MainWindow(QMainWindow):
	def __init__(self, app):
		QMainWindow.__init__(self, None)
		self.setMinimumWidth(int(1280*gui_scale()))
		self.setMinimumHeight(int(768*gui_scale()))
		self.setWindowIcon(get_icon("id"))
		self.setWindowTitle(__app_name__)
		self._data_store = load_data_store()
		self._actions = Actions(self._data_store, self.on_status_message)
		self._selected_identity = None
		self._selected_face = None
		self._state = GuiState()
		self._select_views_button = None
		self.init_actions()
		self.init_ribbon()
		self.init_widgets()
		self.installEventFilter(self)
		if self._data_store.settings.image_library_path != "":
			self._file_tree_dock.set_folder(self._data_store.settings.image_library_path)
			self._find_all_action.setEnabled(True)

		self.read_settings()
		self._ribbon.setVisible(True)
		self._ribbon.setEnabled(True)
		self._ribbon.toggleViewAction().setEnabled(False)

	@property
	def data_store(self):
		return self._data_store

	@property
	def state(self):
		return self._state

	@property
	def actions(self):
		return self._actions

	def init_actions(self):
		self._open_action = self.add_action("Open\nFolder", "folder",
			"Open Folder", True, self.on_open_folder, QKeySequence.Open)
		self._find_all_action = self.add_action("Find\nAll", "zoomfit",
			"Find All", True, self.on_find_all)
		self._find_faces_action = self.add_action("Find\nFaces", "find_faces",
			"Find faces in picture", True, self.on_find_faces)
		self._add_id_action = self.add_action("Add new\nPerson", "add_id",
			"Add new person", True, self.on_add_new_id)
		self._remove_id_action = self.add_action("Remove\nPerson", "rem_id",
			"Remove person", True, self.on_remove_id)
		self._tag_id_action = self.add_action("Tag\nPerson", "tag_id",
			"Tag person", True, self.on_tag_id)
		self._remove_face_action = self.add_action("Remove\nTag", "rem_face",
			"Remove tag from image", True, self.on_remove_face)
		self._add_category_action = self.add_action("Add new\nCategory", "add_category",
			"Add new category", True, self.on_add_category)
		self._remove_category_action = self.add_action("Remove\nCategory", "rem_category",
			"Remove category", True, self.on_remove_category)
		self._show_tags_action = self.add_action("Show\nTags", "show_tag",
	 		"Show the tags that are registered on faces", True, self.on_show_tags, checkable=True)
		self._zoom_fit_action = self.add_action("Zoom\nFit", "zoomfit",
			"Zoom view to fit content", True, self.on_zoom_fit)
		self._select_views_action = self.add_action("Select\nViews", "views",
			"Select view that are visble", True, self.on_select_views)
		self.about_action = self.add_action("About\nApp", QStyle.SP_MessageBoxInformation,
			"About this application", True, self.on_about)

		self._remove_id_action.setEnabled(False)
		self._remove_category_action.setEnabled(False)
		self._find_all_action.setEnabled(False)
		self._remove_face_action.setEnabled(False)

	def init_ribbon(self):
		self._ribbon = QToolBar(self)
		if is_dark_theme():
			self._ribbon.setStyleSheet(get_stylesheet("ribbon_dark"))
		else:
			self._ribbon.setStyleSheet(get_stylesheet("ribbon"))
		self._ribbon.setObjectName("ribbonWidget")
		self._ribbon.setWindowTitle("Ribbon")
		self.addToolBar(self._ribbon)
		self._ribbon.setMovable(False)
		self._ribbon_widget = RibbonWidget(self)
		self._ribbon_widget.currentChanged.connect(self.on_ribbon_changed)
		self._ribbon.addWidget(self._ribbon_widget)

		self.init_home_tab()

	def init_home_tab(self):
		home_tab = self._ribbon_widget.add_ribbon_tab("Home")
		file_pane = home_tab.add_ribbon_pane("File")
		file_pane.add_ribbon_widget(RibbonButton(self, self._open_action, True))
		#file_pane.add_ribbon_widget(RibbonButton(self, self._find_all_action, True))
		identity_pane = home_tab.add_ribbon_pane("Folk")
		identity_pane.add_ribbon_widget(RibbonButton(self, self._find_faces_action, True))
		identity_pane.add_ribbon_widget(RibbonButton(self, self._add_id_action, True))
		identity_pane.add_ribbon_widget(RibbonButton(self, self._remove_id_action, True))
		identity_pane.add_ribbon_widget(RibbonButton(self, self._tag_id_action, True))
		identity_pane.add_ribbon_widget(RibbonButton(self, self._remove_face_action, True))
		category_pane = home_tab.add_ribbon_pane("Category")
		category_pane.add_ribbon_widget(RibbonButton(self, self._add_category_action, True))
		category_pane.add_ribbon_widget(RibbonButton(self, self._remove_category_action, True))
		view_pane = home_tab.add_ribbon_pane("View")
		view_pane.add_ribbon_widget(RibbonButton(self, self._show_tags_action, True))
		view_pane.add_ribbon_widget(RibbonButton(self, self._zoom_fit_action, True))
		self._select_views_button = view_pane.add_ribbon_widget(RibbonButton(self, self._select_views_action, True))
		view_pane.add_ribbon_widget(RibbonButton(self, self.about_action, True))

	def init_widgets(self):
		self._file_tree_dock = FileTreeDock(self)
		self.addDockWidget(Qt.LeftDockWidgetArea, self._file_tree_dock)

		self._project_tree_dock = ProjectTreeDock(self)
		self.addDockWidget(Qt.LeftDockWidgetArea, self._project_tree_dock)

		self._properties_dock = PropertiesDock(self)
		self.addDockWidget(Qt.RightDockWidgetArea, self._properties_dock)

		self._main_widget = PaintedMainWidget(self)
		self.setCentralWidget(self._main_widget)

		self.statusBar().showMessage("Ready")
		self._progress_bar = QProgressBar()
		self._progress_bar.setTextVisible(False)
		self.statusBar().addPermanentWidget(self._progress_bar, 0)

	def add_action(self, caption, icon_name, status_tip, icon_visible, connection, shortcut=None, checkable=False) -> QAction:
		action = QAction(get_icon(icon_name), tr(caption, "ribbon"), self)
		action.setStatusTip(tr(status_tip, "ribbon"))
		action.triggered.connect(connection)
		action.setIconVisibleInMenu(icon_visible)
		if shortcut is not None:
			action.setShortcuts(shortcut)
		action.setCheckable(checkable)
		self.addAction(action)
		return action

	def on_ribbon_changed(self):
		pass

	def on_open_folder(self):
		path = QFileDialog.getExistingDirectory(self, tr("Open Directory"), os.path.expanduser("~"),
																						QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks);
		if path != "":
			self._file_tree_dock.set_folder(path)
			self._data_store.settings.image_library_path = path
			self._find_all_action.setEnabled(True)


	def on_file_tree_selection_changed(self, paths):
		images = []
		for path in paths:
			file_item = self._actions.get_file_item(path)
			if type(file_item) is Image:
				images.append(file_item)
		if len(images) > 0:
			self.centralWidget().show_picture(images[len(images)-1])
			self._properties_dock.on_data_items_selected(images)
		#self._selected_images = images

	def on_project_tree_selection_changed(self, selected_item):
		self._remove_id_action.setEnabled(False)
		self._remove_category_action.setEnabled(False)
		self._remove_face_action.setEnabled(False)
		if type(selected_item) is ItemReference:
			selected_item = selected_item.get_item()
			if type(selected_item) is Face:
				self._selected_face = selected_item
				selected_item = selected_item.image
				self._remove_face_action.setEnabled(True)
		self._properties_dock.on_data_items_selected([selected_item])
		if type(selected_item) is Identity:
			self._remove_id_action.setEnabled(True)
			self._selected_identity = selected_item
		elif type(selected_item) is Category:
			self._remove_category_action.setEnabled(True)
			self._selected_category = selected_item
		elif type(selected_item) is Image:
			self.centralWidget().show_picture(selected_item)
		elif type(selected_item) is Face:
			self._selected_face = selected_item
			selected_item = selected_item.image
			self._remove_face_action.setEnabled(True)
			self.centralWidget().show_picture(selected_item)

	def on_find_all(self):
		pass

	def on_find_faces(self):
		self._actions.find_faces_in_all_pictures(self._data_store.settings.image_library_path)

	def on_add_new_id(self):
		full_name, ok = QInputDialog.getText(self, "New Person", "Full name")
		if ok:
			self._actions.add_identity(full_name, 28)

	def on_remove_id(self):
		result = QMessageBox.question(self, "Remove Person", "Are you sure that you want to remove " + self._selected_identity.name + " from your library?")
		if result == QMessageBox.Yes:
			self._actions.remove_identity(self._selected_identity)

	def on_remove_face(self):
		image = self._selected_face.image
		self._actions.remove_face(self._selected_face)
		self.centralWidget().show_picture(image)

	def on_tag_id(self):
		self._state.main_view_state = GuiState.Tagging

	def on_add_category(self):
		category_name, ok = QInputDialog.getText(self, "New Category", "Category name")
		if ok:
			self._actions.add_category(category_name, 0)

	def on_remove_category(self):
		result = QMessageBox.question(self, "Remove CAtegory",
			"Are you sure that you want to remove " + self._selected_category.name + " from your library?")
		if result == QMessageBox.Yes:
			self._actions.remove_category(self._selected_category)

	def on_show_tags(self):
		self._state.show_faces = self._show_tags_action.isChecked()
		self._state.show_names = self._show_tags_action.isChecked()
		self._main_widget.update()

	def on_zoom_fit(self):
		self._main_widget._scale = 1.0
		self._main_widget._offset *= 0.0
		self._main_widget.update()

	def get_person_dialog(self):
		dialog = QInputDialog(self)
		dialog.setComboBoxItems(self._data_store.identities.get_names())
		dialog.setComboBoxEditable(True)
		dialog.setLabelText("Person")
		dialog.setWindowTitle("Choose Person")
		result = dialog.exec()
		return result, dialog.textValue()

	def on_image_face_tagged(self, image, pos, size):
		result, text = self.get_person_dialog()
		if result == 1:
			identity = None
			face = self._actions.add_face(image, [pos.x(), pos.y()], [size.width(), size.height()])
			self._main_widget._faces.append(face)
			for identity_item in self._data_store.identities.items:
				if identity_item.name == text:
					identity = identity_item
					break
			if identity is None:
				identity = self._data_store.identities.add_identity(text, 20)
			self._actions.tag_face(face, identity)

	def on_face_tagged(self, face):
		result, text = self.get_person_dialog()
		if result == 1:
			identity = None
			for identity_item in self._data_store.identities.items:
				if identity_item.name == text:
					identity = identity_item
					break
			if identity is None:
				identity = self._data_store.identities.add_identity(text, 20)
			self._actions.tag_face(face, identity)

	def on_select_views(self, ev1):
		pos = self._select_views_button.pos()
		pos = self._select_views_button.mapToGlobal(pos)
		self.createPopupMenu().exec(pos)

	def on_escape(self):
		if self._state.main_view_state == GuiState.Tagging:
			self._state.main_view_state = GuiState.SingleView
			self._main_widget.update()

	def closeEvent(self, event):
		self.write_settings()
		save_data_store(self._data_store)

	def eventFilter(self, obj, event):
		if event.type() == QEvent.KeyPress:
			if event.key() == Qt.Key_Delete:
				pass #self.on_delete()
				return True
			if event.key() == Qt.Key_Control:
				pass # self._states.multi_select = True
			if event.key() == Qt.Key_Escape:
				self.on_escape()
		if event.type() == QEvent.KeyRelease:
			if event.key() == Qt.Key_Control:
				pass # self._states.multi_select = False
		if event.type() == QEvent.GraphicsSceneMouseDoubleClick:
			print("double click")
		return False

	def write_settings(self):
		settings = QSettings("folkfinder", "FolkFinder")
		settings.setValue("exists", True)
		settings.setValue("version", 1)
		settings.setValue("pos", self.pos())
		settings.setValue("size", self.size())
		settings.setValue("maximized", self.isMaximized())
		settings.setValue("docks", self.saveState(1))

	def read_settings(self):
		settings = QSettings("folkfinder", "FolkFinder")
		exists = settings.value("exists", False)
		version = int(settings.value("version", 0))
		if not exists or version < 1:
			settings = QSettings("./PracedruDesign.conf", QSettings.IniFormat)
		pos = settings.value("pos", QPoint(100, 100))
		size = settings.value("size", QSize(1280, 800))
		self.resize(size)
		self.move(pos)
		docks_settings_data = settings.value("docks")
		if docks_settings_data is not None:
			self.restoreState(docks_settings_data, 1)
		if settings.value("maximized") == "true":
			self.showMaximized()

	def on_about(self):
		title = self.windowTitle()
		message = self.windowTitle() + " version " + str(__version__) + "" \
				"\nAn application made and copyrighted by:" \
				"\nMagnus Jørgensen (magnusmj@gmail.com)" \
				"\nPrimož Ajdišek (ajdisek.primoz@gmail.com)" \
				"\n\n" + self.windowTitle() +" is open source software and " \
				"is licensed with MIT license." \
				"\nSource code can be found at: \nhttps://gitlab.com/folkfinder/folkfinder"
		QMessageBox.about(self, title, message)

	def on_status_message(self, message, progress=100):
		self._progress_bar.setValue(progress)
		self.statusBar().showMessage(message)
