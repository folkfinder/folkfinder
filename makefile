default:
	@echo "nothing to do"

install:
	@echo "Installing FolkFinder"
	chmod +x bin/folkfinder
	chmod +x folkfinder_pkg/__main__.py

	if [ ! -d "/opt/folkfinder" ]; then \
		mkdir /opt/folkfinder ; \
	fi

	cp -r * /opt/folkfinder/
	cp folkfinder_pkg/__main__.py /opt/folkfinder/
	cp folkfinder.desktop /usr/share/applications/
	cp bin/folkfinder /usr/bin/
	@echo "Installation Success"

clean:
	@echo "Removing FolkFinder"
	rm -rf /opt/folkfinder
	rm /usr/share/applications/folkfinder.desktop
	rm /usr/bin/folkfinder
	@echo "FolkFinder removed"
